import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os

def plotNucleotides(shortreads, matures, names, totalname):
	"""
	Method for plotting nt percentages of 3' terminal nt. 
	Parameters shortreads, matures, names are lists from all samples in one data set.
	Groups to be visualized are [miRNAs w/start reads, miRNAs w/end reads, miRNAs with both start&end, miRNAs wo/short reads]
	"""

	plotCurrent(shortreads, matures, names, totalname, 3)
	plotCurrent(shortreads, matures, names, totalname, 5)

def plotCurrent(shortreads, matures, names, totalname, prime):

  	#Create data
  	starts, ends, boths, noShortreads = list(), list(), list(), list()
  	for i in range(len(matures)):
  		starts.append(findStartReads(matures[i], shortreads[i], prime))
  		ends.append(findEndReads(matures[i], shortreads[i], prime))
  		boths.append(findStartandEndReads(matures[i], shortreads[i], prime))
  		noShortreads.append(findNoShortreads(matures[i], shortreads[i], prime))

  	startA, startC, startG, startT = list(), list(), list(), list()
  	endA, endC, endG, endT = list(), list(), list(), list()
  	bothA, bothC, bothG, bothT = list(), list(), list(), list()
  	noA, noC, noG, noT = list(), list(), list(), list()

  	for i in range(len(starts)):
  		startA.append(starts[i][0])
  		startC.append(starts[i][1])
  		startG.append(starts[i][2])
  		startT.append(starts[i][3])
  		endA.append(ends[i][0])
  		endC.append(ends[i][1])
  		endG.append(ends[i][2])
  		endT.append(ends[i][3])
  		bothA.append(boths[i][0])
  		bothC.append(boths[i][1])
  		bothG.append(boths[i][2])
  		bothT.append(boths[i][3])
  		noA.append(noShortreads[i][0])
  		noC.append(noShortreads[i][1])
  		noG.append(noShortreads[i][2])
  		noT.append(noShortreads[i][3])


  	#Plot data
  	fig = plt.figure(figsize=(20, 13))
  	ax = fig.add_subplot(111)
  	ind = np.arange(len(names))
  	width = 0.2

  	T = '#33a02c'
  	G = '#b2df8a'
  	C = '#1f78b4'
  	A = '#a6cee3'

  	bottom_current = [0]*len(names)
  	plt.bar(ind-2*width, height=startT, width=width, color=T, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, startT)
  	plt.bar(ind-2*width, height=startG, width=width, color=G, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, startG)
  	plt.bar(ind-2*width, height=startC, width=width, color=C, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, startC)
  	plt.bar(ind-2*width, height=startA, width=width, color=A, bottom=bottom_current, label='A')
  	bottom_current = updateList(bottom_current, startA)
  	maxStart = max(bottom_current)

  	bottom_current = [0]*len(names)
  	plt.bar(ind-width, height=endT, width=width, color=T)
  	bottom_current = updateList(bottom_current, endT)
  	plt.bar(ind-width, height=endG, width=width, color=G, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, endG)
  	plt.bar(ind-width, height=endC, width=width, color=C, bottom=bottom_current, label='C')
  	bottom_current = updateList(bottom_current, endC)
  	plt.bar(ind-width, height=endA, width=width, color=A, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, endA)
  	maxEnd = max(bottom_current)

	bottom_current = [0]*len(names)
  	plt.bar(ind, height=bothT, width=width, color=T)
  	bottom_current=updateList(bottom_current, bothT)
  	plt.bar(ind, height=bothG, width=width, color=G, bottom=bottom_current, label='G')
  	bottom_current = updateList(bottom_current, bothG)
  	plt.bar(ind, height=bothC, width=width, color=C, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, bothC)
  	plt.bar(ind, height=bothA, width=width, color=A, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, bothA)
  	maxBoth = max(bottom_current)

	bottom_current = [0]*len(names)
  	plt.bar(ind+width, height=noT, width=width, color=T, label='U')
  	bottom_current = updateList(bottom_current, noT)
  	plt.bar(ind+width, height=noG, width=width, color=G, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, noG)
  	plt.bar(ind+width, height=noC, width=width, color=C, bottom=bottom_current)
  	bottom_current = updateList(bottom_current, noC)
  	plt.bar(ind+width, height=noA, width=width, color=A, bottom=bottom_current)
  	bottom_current=updateList(bottom_current, noA)
  	bottom_current = updateList(bottom_current, noA)
  	maxNo = max(bottom_current)

  	height = max(maxStart, maxEnd, maxBoth, maxNo)

  	for i in range(len(names)):
  		ax.text(ind[i]-0.6*width, height*1.03, names[i], fontsize=45)


  	ticks = list()
  	ticks.extend(ind)
  	ticks.extend(ind-2*width)
  	ticks.extend(ind-width)
  	ticks.extend(ind+width)
  	ticks.sort()
  	ticklabels = list()
  	
  	for i in ind:
  		ticklabels.append('Start')
  		ticklabels.append('End')
  		ticklabels.append('Both')
  		ticklabels.append('None')

  	plt.subplots_adjust(left=0.15, right=0.85, top=0.9, bottom=0.15)
  	ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,fontsize=35)
  	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.tick_params(axis='y', labelsize=40)
	ax.set_xticks(ticks)
	ax.set_xticklabels(ticklabels, fontsize=40, rotation=40, ha='center')
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.set_ylabel('Nucleotides, RPM', fontsize=40)
	ax.yaxis.labelpad = 30

  	plt.savefig('%s_results/ntPreferences/%s_SR_%dpNTpreferences_rpm.png'%(totalname, totalname, prime))
  	plt.clf()

def updateList(bottoms, data):
	"""
	Help method for updating the baseline of next bucket's bars.
	"""
	newBottoms = list()
	if len(bottoms) != len(data): return
	for i in range(len(data)):
		newBottoms.append(bottoms[i]+data[i])
	return newBottoms


def findStartReads(matures, shortreads, prime):
	"""
	Finds all miRNAs associated with start reads, with t=0.5 rpm for miRNAs and start reads.
	"""
  	stats_rpm = {"A": 0.0, "C":0.0, "G":0.0, "T":0.0}

	for ref in matures:
		for miRNA in matures[ref]:
			if not miRNA.mature or miRNA.count<0.5: continue
			hasShortread = False
			if ref in shortreads:
				for shortie in shortreads[ref]:
					if shortie.mature_id==miRNA.miRNA_id and shortie.count>=0.5 and shortie.position=='start':
						hasShortread = True
			if not hasShortread: continue
			
			if prime==3:
				stats_rpm[miRNA.sequence[-1]] += miRNA.count
			elif prime==5:
				stats_rpm[miRNA.sequence[0]] += miRNA.count

	sizes_rpm = [stats_rpm["A"], stats_rpm["C"], stats_rpm["G"], stats_rpm["T"]]
	return sizes_rpm

def findEndReads(matures, shortreads, prime):
	"""
	Finds all miRNAs associated with end reads, with t=0.5 rpm for miRNAs and start reads.
	"""
  	stats_rpm = {"A": 0.0, "C":0.0, "G":0.0, "T":0.0}

	for ref in matures:
		for miRNA in matures[ref]:
			if not miRNA.mature or miRNA.count<0.5: continue
			hasShortread = False
			if ref in shortreads:
				for shortie in shortreads[ref]:
					if shortie.mature_id==miRNA.miRNA_id and shortie.count>=0.5 and shortie.position=='end':
						hasShortread = True
			if not hasShortread: continue

			if prime==3:
				stats_rpm[miRNA.sequence[-1]] += miRNA.count
			elif prime==5:
				stats_rpm[miRNA.sequence[0]] += miRNA.count

	sizes_rpm = [stats_rpm["A"], stats_rpm["C"], stats_rpm["G"], stats_rpm["T"]]
	return sizes_rpm

def findStartandEndReads(matures, shortreads, prime):
	"""
	Finds all miRNAs associated with both start and end reads, with t=0.5 rpm for both miRNAs and short reads.
	"""
	stats_rpm = {"A": 0.0, "C":0.0, "G":0.0, "T":0.0}

	for ref in matures:
		for miRNA in matures[ref]:
			if not miRNA.mature or miRNA.count<0.5: continue
			hasStartReads = False
			hasEndReads = False
			if ref in shortreads:
				for shortie in shortreads[ref]:
					if shortie.mature_id==miRNA.miRNA_id and shortie.count>=0.5:
						if shortie.position=='start':
							hasStartReads = True
						else:
							hasEndReads = True
			if not hasStartReads or not hasEndReads: continue

			if prime==3:
				stats_rpm[miRNA.sequence[-1]] += miRNA.count
			elif prime==5:
				stats_rpm[miRNA.sequence[0]] += miRNA.count

	sizes_rpm = [stats_rpm["A"], stats_rpm["C"], stats_rpm["G"], stats_rpm["T"]]
	return sizes_rpm


def findNoShortreads(matures, shortreads, prime):
	"""
	Finds all miRNAs not associated with short reads, with t=0.5 rpm for miRNAs and 0 for short reads.
	"""
	stats_rpm = {"A": 0.0, "C":0.0, "G":0.0, "T":0.0}

	for ref in matures:
		for miRNA in matures[ref]:
			if not miRNA.mature or miRNA.count<0.5: continue
			hasShortread = False
			if ref in shortreads:
				for shortie in shortreads[ref]:
					if shortie.mature_id==miRNA.miRNA_id:
						hasShortread = True
			if hasShortread: continue
			
			if prime==3:
				stats_rpm[miRNA.sequence[-1]] += miRNA.count
			elif prime==5:
				stats_rpm[miRNA.sequence[0]] += miRNA.count

	sizes_rpm = [stats_rpm["A"], stats_rpm["C"], stats_rpm["G"], stats_rpm["T"]]
	return sizes_rpm



