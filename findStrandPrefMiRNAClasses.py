import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy import stats 

def findStrandPrefClasses(shortread_list, miRNA_list, names, total_name):
	"""
	Main method.
	Splits all miRNAs into two classes, and plots their corresponding SR-association for each strand.
	"""
	plotShortReads(shortread_list, miRNA_list, names, total_name, 'start')
	plotShortReads(shortread_list, miRNA_list, names, total_name, 'end')


def plotShortReads(shortread_list, miRNA_list, names, total_name, position):
	diffSRp_lowest, diffSRp_highest, diffSRp_both, diffSRrpm_lowest, diffSRrpm_highest, diffSRrpm_both = list(), list(), list(), list(), list(), list()
	eqSRp_lowest, eqSRp_highest, eqSRp_both, eqSRrpm_lowest, eqSRrpm_highest, eqSRrpm_both = list(), list(), list(), list(), list(), list()
	totDiff_lowest, totDiff_highest, totDiff_both, totEq_lowest, totEq_highest, totEq_both = list(), list(), list(), list(), list(), list()
	diffClassLenghts, eqClassLenghts = list(), list()

	for i in range(len(miRNA_list)):
		different_class, equal_class = findMiRNAClassesTotalSum(miRNA_list[i])
		diffClassLenghts.append(len(different_class))
		eqClassLenghts.append(len(equal_class))

		diffSRpercentage = findSRassociationOfClassInPercentage(different_class,shortread_list[i], position)
		diffSRrpm = findSRassociationOfClassInRPM(different_class, shortread_list[i], miRNA_list[i], position)
		eqSRpercentage = findSRassociationOfClassInPercentage(equal_class, shortread_list[i], position)
		eqSRrpm = findSRassociationOfClassInRPM(equal_class, shortread_list[i], miRNA_list[i], position)

		diffSRp_lowest.append(diffSRpercentage[0])
		diffSRp_highest.append(diffSRpercentage[1])
		diffSRp_both.append(diffSRpercentage[2])
		diffSRrpm_lowest.append(diffSRrpm[0])
		diffSRrpm_highest.append(diffSRrpm[1])
		diffSRrpm_both.append(diffSRrpm[2])

		eqSRp_lowest.append(eqSRpercentage[0])
		eqSRp_highest.append(eqSRpercentage[1])
		eqSRp_both.append(eqSRpercentage[2])
		eqSRrpm_lowest.append(eqSRrpm[0])
		eqSRrpm_highest.append(eqSRrpm[1])
		eqSRrpm_both.append(eqSRrpm[2])

		totDiff_lowest.extend(diffSRrpm[0])
		totDiff_highest.extend(diffSRrpm[1])
		totDiff_both.extend(diffSRrpm[2])
		totEq_lowest.extend(eqSRrpm[0])
		totEq_highest.extend(eqSRrpm[1])
		totEq_both.extend(eqSRrpm[2])
		

	plotPercentages(diffSRp_lowest, diffSRp_highest, diffSRp_both, names, total_name, 'Different', diffClassLenghts ,position)
	plotPercentages(eqSRp_lowest, eqSRp_highest, eqSRp_both, names, total_name, 'Equal', eqClassLenghts, position)
	
	for i in range(len(names)):
		plotRegression(diffSRrpm_lowest[i], diffSRrpm_highest[i], diffSRrpm_both[i], total_name, 'Different',position,  name=names[i])
		plotRegression(eqSRrpm_lowest[i], eqSRrpm_highest[i], eqSRrpm_both[i], total_name, 'Equal',position,  name=names[i])

	plotRegression(totDiff_lowest, totDiff_highest, totDiff_both, total_name, 'Different', position)
	plotRegression(totEq_lowest, totEq_highest, totEq_both, total_name, 'Equal', position)


def plotPercentages(lowest, highest, both, names, total_name, mirna_class,classLengths, position):
	"""
	Plots the SRassociation percentages for all strand options pr class
	"""
	if total_name=='Lundbaek' or total_name=='Corry':
		fig = plt.figure(figsize=(15,10))
	else:
		fig = plt.figure()
	ax = fig.add_subplot(111)
	ind = np.arange(len(names))
	width = 0.2

	SR = '#1f78b4'
	total = '#a6cee3'
	top_data = list()
	for i in range(len(names)):
		top_data.append(100)

	#Plot total 100% bars
	plt.bar(ind-width, top_data, width=width, color=total)
	plt.bar(ind, top_data, width=width, color=total)
	plt.bar(ind+width, top_data, width=width, color=total)

	#Plot SR percentage bars
	plt.bar(ind-width, lowest, width=width, color=SR)
	plt.bar(ind, highest, width=width, color=SR)
	plt.bar(ind+width, both, width=width, color=SR)

	ticks = list()
	ticks.extend(ind-width)
	ticks.extend(ind)
	ticks.extend(ind+width)
	ticks.sort()
	ticklabels = list()
  
  	for i in ind:
  		ticklabels.append('Lowest')
  		ticklabels.append('Highest')
  		ticklabels.append('Both')

	for i in range(len(names)):
		if total_name=='Lundbaek':
			ax.text(ind[i]-1.4*width, 103, '%s: n=%d'%(names[i], classLengths[i]), fontsize=10)
		elif total_name=='Corry':
			ax.text(ind[i]-width, 104, '%s: n=%d'%(names[i], classLengths[i]), fontsize=20)
		else:
			ax.text(ind[i]-0.5*width, 103, '%s: n=%d'%(names[i], classLengths[i]), fontsize=20)

	
	if total_name=='Lundbaek':
		ax.margins(0.01, None)
		ax.set_xticklabels(ticklabels, rotation=90, ha='left',fontsize=10)
		ax.tick_params(axis='y', labelsize=12)
		ax.set_ylabel('% of hairpins associated with short reads', fontsize=20)
		plt.subplots_adjust(left=0.1, right=0.95, top=0.87, bottom=0.15)
	elif total_name=='Corry':
		ax.margins(0.01, None)
		ax.set_xticklabels(ticklabels, rotation=40, ha='center',fontsize=20)
		ax.tick_params(axis='y', labelsize=20)
		ax.set_ylabel('% of hairpins associated with short reads', fontsize=25)
		plt.subplots_adjust(left=0.1, right=0.95, top=0.87, bottom=0.15)
	else:
		ax.set_xticklabels(ticklabels, rotation=40, ha='center', fontsize=20)
		ax.tick_params(axis='y', labelsize=20)
		ax.set_ylabel('% of hairpins associated with short reads', fontsize=20)
		plt.subplots_adjust(left=0.15, right=0.95, top=0.87, bottom=0.15)
	
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.set_xticks(ticks)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.yaxis.labelpad = 8
	plt.savefig('%s_results/miRNAClasses/%s_%s_percentage_%s.png' % (total_name, total_name, mirna_class, position))
	plt.clf()


def plotRegression(lowest, highest, both, total_name, mirna_class, position, name=None):
	"""
	Plots the SRassociation in RPM values as a regression plot.
	"""	
	l,h,b = True, True, True
	try:
		yl, xl = zip(*lowest)
	except:
		yl,xl = [],[]
		l=False
	try:
		yh, xh = zip(*highest)
	except:
		yh,xh = [],[]
		h=False
	try:	
		yb, xb = zip(*both)
	except:
		yb,xb = [],[]
		b=False

	fig = plt.figure()
	ax = fig.add_subplot(111)
	if l: 
		slope, intercept, r_value, p_value, std_err = stats.linregress(xl,yl)
		line = [slope*xi + intercept for xi in xl]
		ax.plot(xl,line,c='#08519c', label='Lowest\nr=%f\np=%f\ns=%f'%(r_value, p_value, slope))
		ax.scatter(xl, yl, alpha=0.3, s=70, c='#4292c6')
	
	if h: 
		slope, intercept, r_value, p_value, std_err = stats.linregress(xh,yh)
		line = [slope*xi + intercept for xi in xh]
		ax.plot(xh,line,c='#b2182b', label='Highest\nr=%f\np=%f\ns=%f'%(r_value, p_value, slope))
		ax.scatter(xh,yh, alpha=0.3, s=70, c='#f4a582')
	
	if b:
		slope, intercept, r_value, p_value, std_err = stats.linregress(xb,yb)
		line = [ slope*xi + intercept for xi in xb]
		ax.plot(xb,line,c='#1b7837', label='Both\nr=%f\np=%f\ns=%f'%(r_value, p_value, slope))
		ax.scatter(xb, yb, alpha=0.3, s=70, c='#a6dba0')
	
	ax.legend(fontsize=20)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='x', labelsize=20)
	ax.tick_params(axis='y', labelsize=20)
	legend = ax.legend(bbox_to_anchor=(1, 1), loc=2, borderaxespad=0., labelspacing=1)
	try:
		for label in legend.get_lines():
			label.set_linewidth(1.5)
	except AttributeError:
		print 'Catched one error in findStrandPrefClasses'
		
	plt.subplots_adjust(left=0.15, right=0.75, top=0.85, bottom=0.15)
	ax.set_ylabel('Log$_2$ short read count', fontsize=25)
	ax.set_xlabel('Log$_2$ hairpin count', fontsize=25)
	if name!=None:
		plt.savefig('%s_results/miRNAClasses/%s_%s_%s_rpm_%s.png'%(total_name, total_name, name, mirna_class, position))
	else:
		plt.savefig('%s_results/miRNAClasses/%s_%s_rpm_%s.png'%(total_name, total_name, mirna_class, position))
	plt.clf()

def findMiRNAClassesHighest(miRNAs):
	"""
	Classifies hairpins based on the highest miRNA from each strand.
	Returns two classes, 'Different' and 'Equal'
	"""
	different_class, equal_class = dict(), dict()
	for ref in miRNAs:
		p5cand, p3cand = None, None
		for miRNA in miRNAs[ref]:
			if miRNA.prime==5 and miRNA.mature:
				p5cand = miRNA.count
			elif miRNA.prime==3 and miRNA.mature:
				p3cand = miRNA.count
		if p5cand==None or p3cand==None:
			continue
	
		diff = max(p5cand,p3cand)/min(p5cand,p3cand)

		strand = 3 if max(p5cand,p3cand)==p3cand else 5
		if diff<10:
			equal_class[ref] = strand
		else:
			different_class[ref] = strand
	return different_class, equal_class

def findMiRNAClassesTotalSum(miRNAs):
	"""
	Classifies hairpins based on the sum of miRNAs on each strand.
	Returns two classes, 'Different' and 'Equal'
	"""
	different_class, equal_class = dict(), dict()
	for ref in miRNAs:
		p5total, p3total = 0.0, 0.0
		for miRNA in miRNAs[ref]:
			if miRNA.prime==5 and miRNA.count>0.5:
				p5total += miRNA.count
			elif miRNA.prime==3 and miRNA.count>0.5:
				p3total += miRNA.count
		if p5total==0.0 or p3total==0.0:
			continue
		diff = max(p5total,p3total)/min(p5total,p3total)
		strand = 3 if max(p5total,p3total)==p3total else 5		
		if diff<10:
			equal_class[ref] = strand
		else:
			different_class[ref] = strand
	return different_class, equal_class

def findMiRNAClassesTotalAvg(miRNAs):
	"""
	Classifies hairpins based on the average expression level if miRNAs from each strand.
	Returns two classes, 'Different' and 'Equal'
	"""
	different_class, equal_class = dict(), dict()
	for ref in miRNAs:
		p5total, p3total = list(), list()
		for miRNA in miRNAs[ref]:
			if miRNA.prime==5 and miRNA.count>0.0:
				p5total.append(miRNA.count)
			elif miRNA.prime==3 and miRNA.count>0.0:
				p3total.append(miRNA.count)
		if p5total==[] or p3total==[]:
			continue
		avg5 = sum(p5total)/len(p5total)
		avg3 = sum(p3total)/len(p3total)
		diff = max(avg5,avg3)/min(avg5,avg3)
		strand = 3 if max(avg5,avg3)==avg3 else 5		
		if diff<10:
			equal_class[ref] = strand
		else:
			different_class[ref] = strand
	return different_class, equal_class

def findSRassociationOfClassInPercentage(miRNA_class, shortreads, pos):
	"""
	Find SR-association of given class, returned as a percentage for the lowest, highest and both strands.
	"""
	SRlowest, SRhighest, SRboth = 0.0,0.0,0.0

	for ref in miRNA_class:
		if ref not in shortreads:
			continue
		low, high = False, False
		for shortie in shortreads[ref]:
			if shortie.position!=pos or shortie.count<0.5:
				continue
			if shortie.prime==miRNA_class[ref]:
				high = True
			else:
				low = True
		if high and low:
			SRboth += 1
		elif high:
			SRhighest += 1
		elif low:
			SRlowest += 1
	total = len(miRNA_class)
	return 100*SRlowest/total, 100*SRhighest/total, 100*SRboth/total


def findSRassociationOfClassInRPM(miRNA_class, shortreads, miRNAs, pos):
	"""
	Find total expression of shortreads for hairpin in given class.
	Return a list of tuples of SRexpression and Hairpinexpression for lowest, highest and both strands.
	"""
	SRlowest, SRhighest, SRboth = list(), list(), list()
	for ref in miRNA_class:
		if ref not in shortreads:
			continue
		SRlow, SRhigh = 0.0, 0.0
		miRNAlow, miRNAhigh = 0.0,0.0
		for shortie in shortreads[ref]:
			if shortie.position!=pos or shortie.count<0.5:
				continue
			if shortie.prime==miRNA_class[ref]:
				SRhigh += shortie.count
			else:
				SRlow += shortie.count
		for miRNA in miRNAs[ref]:
			if miRNA.prime==miRNA_class[ref] and miRNA.count>=0.5:
				miRNAhigh += miRNA.count
			elif miRNA.count>=0.5:
				miRNAlow += miRNA.count

		if SRlow>0.0 and SRhigh>0.0:
			SRboth.append((math.log(SRlow,2), math.log(miRNAlow,2)))
			SRboth.append((math.log(SRhigh,2), math.log(miRNAhigh,2)))
		elif SRlow>0.0:
			SRlowest.append((math.log(SRlow,2), math.log(miRNAlow,2)))
		elif SRhigh>0.0:
			SRhighest.append((math.log(SRhigh,2), math.log(miRNAhigh,2)))
	return SRlowest, SRhighest, SRboth


