import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np

def plotLengths(shortreads, name, totalname):
	"""
	Accepts a short read map and plots the RPM-value for every length.
	Splits in start and end reads.
	"""
	start_values, end_values, keys = createData(shortreads)
	plotStartreads(keys, start_values, name, totalname)
	plotEndreads(keys, end_values, name, totalname)


def plotEndreads(end_keys, end_values, name, totalname):
	"""
	Plots values for end reads
	"""
	ind = np.arange(len(end_keys))
	fig = plt.figure()
	ax = fig.add_subplot(111)
	width = 0.7
	ax.bar(ind, end_values, width=width, color='#3182bd')
	plt.subplots_adjust(left=0.17, right=0.9, top=0.9, bottom=0.15)
	ax.yaxis.labelpad = 15
	ax.set_xticks(ind+0.5*width, minor=False)
	ax.set_xticklabels(end_keys, fontsize=20)
	ax.tick_params(axis='y', labelsize=20)
	ax.set_xlabel('Length of end read', fontsize=25)
	ax.set_ylabel('RPM', fontsize=25)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	plt.savefig('%s_results/SRvsLength/%s_%s_EndreadRPMvsLength.png'%(totalname, totalname, name))
	plt.clf()

def plotStartreads(start_keys, start_values, name, totalname):
	"""
	Plots values for start reads
	"""
	ind = np.arange(len(start_keys))
	fig = plt.figure()
	ax = fig.add_subplot(111)
	width = 0.7
	ax.bar(ind, start_values, width=width, color='#3182bd')
	plt.subplots_adjust(left=0.17, right=0.9, top=0.9, bottom=0.15)
	ax.yaxis.labelpad = 15
	ax.set_xticks(ind+0.5*width, minor=False)
	ax.set_xticklabels(start_keys, fontsize=20)
	ax.tick_params(axis='y', labelsize=20)
	ax.set_xlabel('Length of start read', fontsize=25)
	ax.set_ylabel('RPM', fontsize=25)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	plt.savefig('%s_results/SRvsLength/%s_%s_StartreadRPMvsLength.png'%(totalname, totalname, name))
	plt.clf()



def createData(shortreads):
	"""
	Creates the data to be used in the plot.
	Returns data for start and end reads.
	Threshold of rpm=0.5
	"""
	start_data, end_data = {11:.0,12:.0,13:.0,14:.0,15:.0}, {11:.0,12:.0,13:.0,14:.0,15:.0}
	start_values, end_values = list(), list()

	for ref in shortreads:
		for shortie in shortreads[ref]:
			if shortie.count<0.5:
				continue
			if shortie.position=='start':
				length = len(shortie.sequence)
				start_data[length] += shortie.count
			elif shortie.position=='end':
				length = len(shortie.sequence)
				end_data[length] += shortie.count
			else:
				print 'Something wrong. Position for this short read is: %s'%shortie.position

	keys = [11,12,13,14,15]
	for ref, value in sorted(start_data.iteritems()):
		if start_data[ref]!=.0:
			newValue = start_data[ref]
			start_values.append(newValue)
		else:
			start_values.append(0.0)

	for ref, value in sorted(end_data.iteritems()):
		if end_data[ref]!=.0:
			newValue = end_data[ref]
			end_values.append(newValue)
		else:
			end_values.append(0.0)
	
	return start_values, end_values, keys
