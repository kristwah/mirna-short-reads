import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np
import os


def plotRegression(matures, shortreads, names, total_name):
	savepath = total_name+'_results/Regression'
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	print 'Plotting regressions...'

	data_tot, data_start, data_end, data_highest, data_lowest = findTotData(shortreads, matures)
	plot(data_tot, 'log$_2$ short read counts', '%s short reads vs. mature sequences' % total_name, '%s/%s_SRvsMature'%(savepath, total_name))
	plot(data_start, 'log$_2$ start read counts', '%s start reads vs. mature sequences'%total_name, '%s/%s_StartvsMature'%(savepath, total_name))
	plot(data_end, 'log$_2$ end read counts', '%s end reads vs. mature sequences'%total_name, '%s/%s_EndvsMature'%(savepath,total_name))
	plot(data_highest, 'log$_2$ short read counts', '%s short reads vs. mature sequences on highest strand'%total_name, '%s/%s_SRvsMature_Higheststrand'%(savepath, total_name))
	plot(data_lowest, 'log$_2$ short read counts', '%s short reads vs. mature sequences on lowest strand'%total_name, '%s/%s_SRvsMature_Loweststrand'%(savepath,total_name))
	
	for i in range(len(names)):
		data_toti, data_starti, data_endi, data_highesti, data_lowesti = findTotData([shortreads[i]], [matures[i]])
		plot(data_toti, 'log$_2$ short read counts', '%s %s short reads vs. mature sequences' % (total_name, names[i]), '%s/%s_%s_SRvsMature'%(savepath, total_name, names[i]))
		plot(data_starti, 'log$_2$ start read counts', '%s %s start reads vs. mature sequences'%(total_name, names[i]), '%s/%s_%s_StartvsMature'%(savepath, total_name, names[i]))
		plot(data_endi, 'log$_2$ end read counts', '%s %s end reads vs. mature sequences'%(total_name, names[i]), '%s/%s_%s_EndvsMature'%(savepath,total_name,names[i]))
		plot(data_highesti, 'log$_2$ short read counts', '%s %s short reads vs. mature sequences on highest strand'%(total_name, names[i]), '%s/%s_%s_SRvsMature_Higheststrand'%(savepath, total_name, names[i]))
		plot(data_lowesti, 'log$_2$ short read counts', '%s %s short reads vs. mature sequences on lowest strand'%(total_name, names[i]), '%s/%s_%s_SRvsMature_Loweststrand'%(savepath,total_name, names[i]))


def plot(data, label, title, savename):
	"""
	The data are touples of (read_count, mature_count). We want read_count on y-axis and mature_count on x-axis.
	"""
	try:
		y, x = zip(*data)
	except:
		return
	fig = plt.figure()
	ax = fig.add_subplot(111)
	(m,b) = np.polyfit(x,y,1)
	yp = np.polyval([m,b],x)
	ax.plot(x,yp,c='#08519c')
	ax.scatter(x, y, alpha=0.3, s=110, c='#4292c6')
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='x', labelsize=15)
	ax.tick_params(axis='y', labelsize=15)
	ax.set_ylabel(label, fontsize=20)
	ax.set_xlabel('log$_2$ miRNA sequence counts', fontsize=20)
	plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.15)
	plt.savefig('%s.png' % (savename))
	plt.clf()

def findTotData(shortreads, matures):
	data_tot = list()
	for i in range(len(shortreads)):
		newData = findEndData(shortreads[i], matures[i])
		data_tot.extend(newData)
	
	data_start = list() 
	for i in range(len(shortreads)):
		newData = findEndData(shortreads[i], matures[i], pos='end')
		data_start.extend(newData)

	data_end = list() 
	for i in range(len(shortreads)):
		newData = findEndData(shortreads[i], matures[i], pos='start')
		data_end.extend(newData)

	data_highest = list()
	for i in range(len(shortreads)):
		newData = findEndData(shortreads[i], matures[i], strand=False)
		data_highest.extend(newData)

	data_lowest = list()
	for i in range(len(shortreads)):
		newData = findEndData(shortreads[i], matures[i], strand=True)
		data_lowest.extend(newData)

	return data_tot, data_start, data_end, data_highest, data_lowest


def findEndData(shortreads, miRNAs_mature, pos=None, strand=None):
	ends = dict()
	endsHighest = list()
	noEnds = list()
	noExpr = list()
	notHighest = list()
	onLowestStrand = list()
	shortreads_count = 0
	for ref in shortreads:
		for Shortread in shortreads[ref]:
			if Shortread.position==pos or Shortread.highest_prime==strand:
				continue
			mature = Shortread.mature_id
			count = Shortread.count

			if count<0.5:
				continue

			
			if ends.has_key(mature):
				ends[mature] += count
			else:
				ends[mature] = count

			shortreads_count += count


	data = list()
	for ref in miRNAs_mature:
		for mat in miRNAs_mature[ref]:
			expr = mat.count
			
			if not ends.has_key(mat.miRNA_id) or expr<0.5: 
				continue

			expr2 = math.log(expr, 2)
			end = ends[mat.miRNA_id]
			end2 = math.log(end, 2)
			data.append((end2, expr2))

	return data
				
