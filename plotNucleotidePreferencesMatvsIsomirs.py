import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os

def plotNucleotides(matures, names, totalname):
  """
  Method for plotting nt percentages of 3' terminal nt. 
  Parameters shortreads, matures, names are lists from all samples in one data set.
  Groups to be visualized are [miRNAs w/start reads, miRNAs w/end reads, miRNAs with both start&end, miRNAs wo/short reads]
  """

  plotCurrent(matures, names, totalname, 3)
  plotCurrent(matures, names, totalname, 5)

def plotCurrent(matures, names, totalname, prime):  
  miRNAs, isomirs = list(), list()
  for i in range(len(matures)):
    miRNAs.append(findMatures(matures[i], prime))
    isomirs.append(findIsomirs(matures[i], prime))
  matureA, matureC, matureG, matureT = list(), list(), list(), list()
  isomirA, isomirC, isomirG, isomirT = list(), list(), list(), list()

  for i in range(len(miRNAs)):
    matureA.append(miRNAs[i][0])
    matureC.append(miRNAs[i][1])
    matureG.append(miRNAs[i][2])
    matureT.append(miRNAs[i][3])
    isomirA.append(isomirs[i][0])
    isomirC.append(isomirs[i][1])
    isomirG.append(isomirs[i][2])
    isomirT.append(isomirs[i][3])

  #Plot data
  fig = plt.figure(figsize=(20,13))
  ax=fig.add_subplot(111)
  ind = np.arange(len(names))
  width = 0.3
  T = '#33a02c'
  G = '#b2df8a'
  C = '#1f78b4'
  A = '#a6cee3'
  bottom_current = [0]*len(names)
  plt.bar(ind-width, height=matureT, width=width, color=T, bottom=bottom_current, label='U')
  bottom_current = updateList(bottom_current, matureT)
  plt.bar(ind-width, height=matureG, width=width, color=G, bottom=bottom_current, label='G')
  bottom_current = updateList(bottom_current, matureG)
  plt.bar(ind-width, height=matureC, width=width, color=C, bottom=bottom_current, label='C')
  bottom_current = updateList(bottom_current, matureC)
  plt.bar(ind-width, height=matureA, width=width, color=A, bottom=bottom_current, label='A')

  bottom_current = [0]*len(names)
  plt.bar(ind, height=isomirT, width=width, color=T)
  bottom_current = updateList(bottom_current, isomirT)
  plt.bar(ind, height=isomirG, width=width, color=G, bottom=bottom_current)
  bottom_current = updateList(bottom_current, isomirG)
  plt.bar(ind, height=isomirC, width=width, color=C, bottom=bottom_current)
  bottom_current = updateList(bottom_current, isomirC)
  plt.bar(ind, height=isomirA, width=width, color=A, bottom=bottom_current)
  bottom_current = updateList(bottom_current, isomirA)

  height = bottom_current[0]

  for i in range(len(names)):
    ax.text(ind[i]-width, height*1.03, names[i], fontsize=45)

  ticks = list()
  ticks.extend(ind-width)
  ticks.extend(ind)
  ticks.sort()
  ticklabels = list()

  for i in ind:
    ticklabels.append('MiRNAs')
    ticklabels.append('Isomirs')

  plt.ylim(0,100)
  plt.subplots_adjust(left=0.15, right=0.8, top=0.9, bottom=0.15)
  ax.yaxis.labelpad = 10
  handles, labels = ax.get_legend_handles_labels()
  labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
  ax.legend(handles, labels, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.,fontsize=40)
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.tick_params(axis='y', labelsize=40)
  ax.set_xticks(ticks)
  ax.set_xticklabels(ticklabels, fontsize=40, rotation=40, ha='center')
  ax.xaxis.set_ticks_position('bottom')
  ax.yaxis.set_ticks_position('left')
  ax.set_ylabel('Nucleotides, %', fontsize=50)
  plt.savefig('%s_results/ntPreferences/%s_isomirs_%dpNTpreferences.png'%(totalname, totalname,prime))
  plt.clf()

def updateList(bottoms, data):
	"""
	Help method for updating the baseline of next bucket's bars.
	"""
	newBottoms = list()
	if len(bottoms) != len(data): return
	for i in range(len(data)):
		newBottoms.append(bottoms[i]+data[i])
	return newBottoms

def findMatures(matures, prime):
  """
  Finds all mature miRNAs, with t=0.5 rpm
  """
  stats_rpm =  {"A": 0.0, "C":0.0, "G":0.0, "T":0.0}
  for ref in matures:
    for miRNA in matures[ref]:
      if miRNA.mature and miRNA.count>=0.5:
        if prime==3:
          stats_rpm[miRNA.sequence[-1]] += miRNA.count
        elif prime==5:
          stats_rpm[miRNA.sequence[0]] += miRNA.count
  tot = stats_rpm["A"]+stats_rpm["C"]+stats_rpm["G"]+stats_rpm["T"]
  if tot==0.0:
    sizes_rpm=[.0,.0,.0,.0]
  else:
    sizes_rpm = [100*stats_rpm["A"]/tot, 100*stats_rpm["C"]/tot, 100*stats_rpm["G"]/tot, 100*stats_rpm["T"]/tot]
  return sizes_rpm


def findIsomirs(matures, prime):
  stats_rpm = {"A":.0,"C":.0,"G":.0,"T":.0}
  for ref in matures:
    for miRNA in matures[ref]:
      if miRNA.mature or miRNA.count<0.5:
        continue
      if prime==3:
        stats_rpm[miRNA.sequence[-1]] += miRNA.count
      elif prime==5:
        stats_rpm[miRNA.sequence[0]] += miRNA.count
  tot = stats_rpm["A"]+stats_rpm["C"]+stats_rpm["G"]+stats_rpm["T"]
  if tot==0.0:
    sizes_rpm = [.0,.0,.0,.0]
  else:
    sizes_rpm = [100*stats_rpm["A"]/tot, 100*stats_rpm["C"]/tot, 100*stats_rpm["G"]/tot, 100*stats_rpm["T"]/tot]
  return sizes_rpm

