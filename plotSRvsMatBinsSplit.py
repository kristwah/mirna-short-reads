import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np

def plotBins(matures, shortreads, name, total_name):
	"""
	Method for plotting SR-associated miRNAs as a binned histogram.
	"""
	bins = createBins(matures)
	plot3p(bins, shortreads, name, total_name)
	plot5p(bins, shortreads, name, total_name)
	plotHighest(bins, shortreads, name, total_name)
	plotLowest(bins, shortreads, name, total_name)


def createBins(matures):
	"""
	Method for calculating the bins.
	"""
	miRNAs = list()

	for ref in matures:
		for miRNA in matures[ref]:
			if miRNA.count>=0.5:
				miRNAs.append([miRNA.count, miRNA])

	miRNAs.sort()
	splitsize = 1.0/10*len(miRNAs)
	bins = [(miRNAs[int(round(i*splitsize)):int(round((i+1)*splitsize))]) for i in range(10)]

	return bins

def plotHighest(bins, shortreads, name, total_name):
	"""
	Method for finding the number of miRNAs for each group, and the number of miRNAs with short reads within each group.
	"""
	shortread_count, miRNA_count = list(), list()
	binsize = len(bins[0])

	for bin in bins:
		miRNA_counts = 0.0
		SR_count = 0.0
		for item in bin:
			if not item[1].highest_prime:
				continue
			miRNA_counts += 1
			SR = False
			if item[1].hairpin_id in shortreads:
				for shortie in shortreads[item[1].hairpin_id]:
					if shortie.mature_id == item[1].miRNA_id and shortie.count>=0.5:
						SR = True
			if SR:	SR_count += 1
		shortread_count.append((SR_count/binsize*100))		 
		miRNA_count.append(miRNA_counts/binsize*100)

	topData = [100,100,100,100,100,100,100,100,100,100]
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ind = np.arange(10)

	plt.bar(ind, height=topData, color='#c6dbef')
	plt.bar(ind, height=miRNA_count, color='#045a8d', alpha=0.5)
	plt.bar(ind, height=shortread_count, color='#034e7b', alpha=0.7)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.set_xticklabels([])
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='y', labelsize=20)
	ax.yaxis.labelpad = 20
	plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.1)
	ax.set_ylabel('SR-association of miRNAs, %', fontsize=25)
	ax.set_xlabel('low        miRNA expression      high', fontsize=25)
	plt.savefig('%s_results/Bins/%s_%s_SRvsMat_bins_high.png' % (total_name, total_name,name))
	plt.clf()


def plotLowest(bins, shortreads, name, total_name):
	"""
	Method for finding the number of miRNAs for each group, and the number of miRNAs with short reads within each group.
	"""
	shortread_count, miRNA_count = list(), list()
	binsize = len(bins[0])

	for bin in bins:
		miRNAs = 0.0
		SR_count = 0.0
		for item in bin:
			if item[1].highest_prime:
				continue
			miRNAs += 1
			SR = False
			if item[1].hairpin_id in shortreads:
				for shortie in shortreads[item[1].hairpin_id]:
					if shortie.mature_id == item[1].miRNA_id and shortie.count>=0.5:
						SR = True
			if SR:
				SR_count += 1
		shortread_count.append((SR_count/binsize*100))
		miRNA_count.append((miRNAs/binsize*100))

	#topData = [binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize]
	topData = [100,100,100,100,100,100,100,100,100,100]

	fig = plt.figure()
	ax = fig.add_subplot(111)
	ind = np.arange(10)

	plt.bar(ind, height=topData, color='#c6dbef')
	plt.bar(ind, height=miRNA_count, color='#045a8d', alpha=0.5)
	plt.bar(ind, height=shortread_count, color='#034e7b', alpha=0.7)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.set_xticklabels([])
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='y', labelsize=20)
	ax.yaxis.labelpad = 20
	plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.1)
	ax.set_ylabel('SR-association of miRNAs, %', fontsize=25)
	ax.set_xlabel('low        miRNA expression      high', fontsize=25)
	plt.savefig('%s_results/Bins/%s_%s_SRvsMat_bins_low.png' % (total_name, total_name, name))
	plt.clf()

def plot3p(bins, shortreads, name, total_name):
	"""
	Method for finding the number of miRNAs for each group, and the number of miRNAs with short reads within each group.
	"""
	shortread_count, miRNA_count = list(), list()
	binsize = len(bins[0])

	for bin in bins:
		miRNAs = 0.0
		SR_count = 0.0
		for item in bin:
			if item[1].prime==5:
				continue
			miRNAs += 1
			SR = False
			if item[1].hairpin_id in shortreads:
				for shortie in shortreads[item[1].hairpin_id]:
					if shortie.mature_id == item[1].miRNA_id and shortie.count>=0.5:
						SR = True
			if SR:
				SR_count += 1
		shortread_count.append((SR_count/binsize*100))
		miRNA_count.append((miRNAs/binsize*100))

	#topData = [binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize]
	topData = [100,100,100,100,100,100,100,100,100,100]


	fig = plt.figure()
	ax = fig.add_subplot(111)
	ind = np.arange(10)

	plt.bar(ind, height=topData, color='#c6dbef')
	plt.bar(ind, height=miRNA_count, color='#045a8d', alpha=0.5)
	plt.bar(ind, height=shortread_count, color='#034e7b', alpha=0.7)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.set_xticklabels([])
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='y', labelsize=20)
	ax.yaxis.labelpad = 20
	plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.1)
	ax.set_ylabel('SR-association of miRNAs, %', fontsize=25)
	ax.set_xlabel('low        miRNA expression      high', fontsize=25)
	plt.savefig('%s_results/Bins/%s_%s_SRvsMat_bins_3p.png' % (total_name, total_name, name))
	plt.clf()


def plot5p(bins, shortreads, name, total_name):
	"""
	Method for finding the number of miRNAs for each group, and the number of miRNAs with short reads within each group.
	"""
	shortread_count, miRNA_count = list(), list()
	binsize = len(bins[0])

	for bin in bins:
		miRNAs = 0.0
		SR_count = 0.0
		for item in bin:
			if item[1].prime==3:
				continue
			miRNAs += 1
			SR = False
			if item[1].hairpin_id in shortreads:
				for shortie in shortreads[item[1].hairpin_id]:
					if shortie.mature_id == item[1].miRNA_id and shortie.count>=0.5:
						SR = True
			if SR:
				SR_count += 1
		shortread_count.append((SR_count/binsize*100))
		miRNA_count.append((miRNAs/binsize*100))

	topData = [binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize,binsize]
	topData = [100,100,100,100,100,100,100,100,100,100]


	fig = plt.figure()
	ax = fig.add_subplot(111)
	ind = np.arange(10)

	plt.bar(ind, height=topData, color='#c6dbef')
	plt.bar(ind, height=miRNA_count, color='#045a8d', alpha=0.5)
	plt.bar(ind, height=shortread_count, color='#034e7b', alpha=0.7)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.set_xticklabels([])
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='y', labelsize=20)
	ax.yaxis.labelpad = 20
	plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.1)
	ax.set_ylabel('SR-association of miRNAs, %', fontsize=25)
	ax.set_xlabel('low        miRNA expression      high', fontsize=25)
	plt.savefig('%s_results/Bins/%s_%s_SRvsMat_bins_5p.png' % (total_name, total_name, name))
	plt.clf()

