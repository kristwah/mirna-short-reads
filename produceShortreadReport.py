import sys
import string
import math
import numpy as np
import os
from Bio import SeqIO
import plotSampleSRvsFull as pssf
import calcAnova as ca
import findBins as fb
import findLengths as fl
import findMiRNAClassifications as fmc
import findNucleotidePreferences as fnp
import findAlignments as fa



class Shortread:
	def __init__(self, hairpin_id, offset, sequence, count, mature_id, position, prime, mat_offset, highest_prime):
		self.hairpin_id = hairpin_id
		self.offset = offset
		self.sequence = sequence
		self.count = count
		self.mature_id = mature_id
		self.position = position
		self.prime = prime
		self.mat_offset = mat_offset
		self.highest_prime = highest_prime

class MicroRNA:
	def __init__(self, hairpin_id, miRNA_id, count, offset, sequence, prime):
		self.hairpin_id = hairpin_id
		self.miRNA_id = miRNA_id
		self.count = count
		self.offset = offset
		self.sequence = sequence
		self.prime = prime
		self.expressed = True
		self.highest_prime = None
		self.mature = False

	def setUnexpressed(self):
		self.expressed = False

	def setHighestPrime(self, highest_prime):
		self.highest_prime = True if self.prime==highest_prime else False

	def setMature(self, matureID):
		if self.miRNA_id==matureID:
			self.mature = True 

class Hairpin:
	def __init__(self, hairpin_id, sequence, foldstructure):
		self.hairpin_id = hairpin_id
		self.sequence = sequence
		self.foldstructure = foldstructure
		self.expressed = False

	def setExpressed(self):
		self.expressed = True


def main(refFile, dotBracketFile, matFile, sampFile, name, total_name):
	"""
	Main method, calls all functions necessary.
	"""
	global hairpin_map, miRNA_unique, miRNA_total, miRNAs_highexpr, shortreads, mors, shortread_map, microRNA_map, bucketSize, unannotated
	# Initialize global data structures
	hairpin_map = dict()
	miRNA_unique  = dict()
	miRNA_total = dict()
	miRNAs_highexpr = dict()
	shortreads = dict()
	mors = dict()
	shortread_map = dict()
	microRNA_map = dict()
	unannotated = dict()
	bucketSize = 5

	#Load input data
	readHairpins(refFile, dotBracketFile)
	readMatures(matFile)
	readSample(sampFile)
	
	#Calculations. 
	#If mature mode, use alignShortreads(). If isomiR mode, use alignShortreadsToIsomirs(). NB! Last function call of readSample() must be altered accordingly.
	#alignShortreadsToIsomirs()
	alignShortreads()
	removeUnexpressed()

	return shortread_map, microRNA_map, hairpin_map

def readHairpins(hairpinfile, foldfile):
	"""
	Load hairpin file using biopython.
	Load dot-bracket file.
	"""
	global hairpin_map
	f = open(hairpinfile)
	folds = readFoldstructures(foldfile)
	for record in SeqIO.parse(f, 'fasta'):
		try:
			hairpin_map[record.id] = Hairpin(record.id, record.seq, folds[record.id])
			addRefAsKey(record.id, record.seq)
		except KeyError:
			print('Missing fold structure  for: ' + record.id)
			print record.id in folds
			continue

def readFoldstructures(foldfile):
	"""
	Reads file with fold structures for hairpins, and stores the data in a dict.
	"""
	folds = {}
	f = open(foldfile)
	ref, seq = '', ''
	inProcess = False
	
	for line in f:
		line = line.strip('\r\n') #If file is made in Windows OS
		line = line.strip('\n') 
		if line[0]=='>' and inProcess:
			folds[ref] = seq
			ref = line[1:]
			seq = ''
		elif line[0]=='>':
			ref = line[1:]
			folds[ref] = ''
			inProcess = True
		else:
			seq += line
	folds[ref] = seq
	f.close()
	return folds

def readMatures(filename):
	"""
	Reads input file of mature sequences for reference ID's.
	Format of input lines:
	mmu-let-7g	6	TGAGGTAGTAGTTTGTACAGTT	
	"""
	f = open(filename, 'r')
	seq, ref = '',''
	global microRNA_map
	
	for line in f:
		skip = False
		data = line.split()
		ref = data[0]
		offset = int(data[1])
		sequence = data[2]
		if isInvalidSample(ref, offset, sequence): 
			continue
		if ref in hairpin_map:
			#There may be multiple mature seq's pr reference ID, so add a postfix number to mature ID's showing the total count.
			count = len(microRNA_map[ref])
			if count>0:
				for miRNA in microRNA_map[ref]:
					if miRNA.sequence==sequence and miRNA.offset==offset:
						skip = True		
			if not skip:
				newMat = ref + str(count+1)
				newMiRNA = MicroRNA(ref, newMat, 0.0, offset, sequence, findPrime(ref, offset))
				microRNA_map[ref].append(newMiRNA)
	f.close()

	
def addRefAsKey(ref, seq):
	"""
	Adds a new reference ID and it's sequence to all relevant dictionaries.
	"""
	global  miRNA_unique, shortreads
	if ref =='hsa-mir-1234':
		seq = 'GUGAGUGUGGGGUGGCUGGGGCGGGGGGGGCCCGGGGACGGCUUGGGCCUGCCUAGUCGGCCUGACCACCCACCCCACAG'

	miRNA_unique[ref] = countDict(seq)
	miRNA_total[ref] = countDict(seq)
	miRNAs_highexpr[ref] = dict()
	shortreads[ref] = list()
	mors[ref] = list()
	shortread_map[ref] = list()
	microRNA_map[ref] = list()

	
def isSeq(seq):
	"""
	Help method for checking if new sequence is valid.
	"""
	allowed = 'ACGT'
	if seq == '' : return False
	for s in seq.upper():
		if s not in allowed:
			return False
	return True

def isInvalidSample(ref, offset, seq):
	"""
	Help method for checking if new sample and reference ID is valid.
	"""
	if ref not in hairpin_map:
		return True
	elif not isSeq(seq):
		return True
	elif len(hairpin_map[ref].sequence)-offset < len(seq):
		return True
	else:
		return False
	
def countDict (string):
	"""
	Creates dictionaries for indexes and counts of a given sequence's length.
	"""
	l = len(string)
	j=0
	if l%bucketSize > 0: j=1
	counts = dict()
	for i in range (l//bucketSize + j - 1):
		counts[i]=[0]*l	
	return counts
	
def readSample(filename):
	"""
	Reads sample data from given file.
	Updates counts for relevant index ranges in existing reference sequences.
	Format of input lines:
	4-86385	mmu-mir-10b	4	TACCCTGTAGAACCGAATTTGT	0
	"""
	global miRNA_total, miRNA_unique, shortreads, unannotated
	totCount = 0.0
	f = open(filename, 'r')
	ref, seq, offset, count = "", "", 0, 0.0
	for line in f:
		sample = line.split()
		count = int(sample[0].split('-')[1])
		totCount += count
	f.close()
	
	f = open(filename, 'r')
	for line in f:
		sample = line.split()
		count = int(sample[0].split('-')[1])
		nCount = count/(float(sample[4])+1)
		rpm = (nCount / totCount) * 1000000.0
		
		ref = sample[1]
		offset = int(sample[2])
		seq = sample[3]
		if isInvalidSample(ref, offset, seq): 
			ref, seq, offset, count = '', '', 0, 0.0
			continue
		
		bucketIndex = (len(seq)-1) // bucketSize
		success = True 
		for i in range(offset, offset+len(seq)):
			try:
				miRNA_unique[ref][bucketIndex][i] += 1
				miRNA_total[ref][bucketIndex][i] += rpm
			except:
				success = False
				continue
		
		#If bucketindex is 2 we have a short read
		if bucketIndex==2 and success: 
			shortreads[ref].append([offset, seq, rpm, ''])
		
		#Save count if sequence is a mature seq
		if success and bucketIndex in range(3,5):
			annotated = False
			for miRNA in microRNA_map[ref]:
				if miRNA.offset==offset and miRNA.sequence==seq:
					miRNA.count += rpm
					annotated = True
			if not annotated and len(seq)>18 and rpm>=0.5:
				if ref not in unannotated:
					unannotated[ref] = [[seq,offset,rpm]]
				else:
					unannotated[ref].append([seq,offset,rpm])

	
	#Check if any read was higher expressed than the mature sequences for this reference hairpin
	#If run in mature sequence mode, use findUnannotatedSequences(). If run in isomiR mode, use findIsomirs().
	
	for ref in hairpin_map:
		findUnannotatedSequences(ref)
		#findIsomirs(ref)
	f.close()


def findIsomirs(ref):
	"""
	Alternative to findUnannotatedSequences(), finds all isomirs, not only ones more expressed than the annotated miRNA sequences.
	"""
	global unannotated, microRNA_map, hairpin_map

	#If there are no new potential isomirs for the annotated mature sequence, save the mature sequence if any and set expression level of hairpin if true
	if ref not in unannotated:
		try:
			annotated3 = [miRNA for miRNA in microRNA_map[ref] if miRNA.prime==3][0]
		except IndexError:
			annotated3 = None
		try:
			annotated5 = [miRNA for miRNA in microRNA_map[ref] if miRNA.prime==5][0]
		except IndexError:
			annotated5 = None
		
		if annotated3==None and annotated5==None: return 0
		mostExpr=0
		if (annotated3!=None and annotated5!=None and annotated3.count>annotated5.count) or (annotated3!=None and annotated3.count>0.0):
			mostExpr=3
		elif annotated5!=None and annotated5.count>0.0:
			mostExpr = 5

		for miRNA in microRNA_map[ref]:
			if annotated3!=None and annotated3.count>0.0: miRNA.setMature(annotated3.miRNA_id)
			if annotated5!=None and annotated5.count>0.0: miRNA.setMature(annotated5.miRNA_id)
			if mostExpr!=0: miRNA.setHighestPrime(mostExpr)
		if mostExpr!=0:
			hairpin_map[ref].setExpressed()
		return 0

	news = 0
	for read in unannotated[ref]:
		seq = read[0]
		offset = read[1]
		count = read[2]
		prime = findPrime(ref, offset)
		newID = '%s%d3N'%(ref, (len(microRNA_map[ref])+1)) if prime==3 else '%s%d5N'%(ref, (len(microRNA_map[ref])+1))
		microRNA_map[ref].append(MicroRNA(ref, newID, count, offset, seq, prime))
		news += 1
		

	candidates3 = [(miRNA.count, miRNA.miRNA_id) for miRNA in microRNA_map[ref] if miRNA.prime==3 and miRNA.count>0.0]
	candidates5 = [(miRNA.count, miRNA.miRNA_id) for miRNA in microRNA_map[ref] if miRNA.prime==5 and miRNA.count>0.0]	
	highest3 = max(candidates3) if candidates3!=[] else [0.0,'']
	highest5 = max(candidates5) if candidates5!=[] else [0.0,'']

	
	
	mostExpr = 0
	if highest3[0]>highest5[0]:
		mostExpr=3
	elif highest5>0.0:
		mostExpr=5

	for miRNA in microRNA_map[ref]:
		if highest3[1]!='': miRNA.setMature(highest3[1])
		if highest5[1]!='': miRNA.setMature(highest5[1])
		if mostExpr!=0: miRNA.setHighestPrime(mostExpr)

	if mostExpr!=0:
		hairpin_map[ref].setExpressed()
	
	return news


def findUnannotatedSequences(ref):
	"""
	Alternative to 'checkForNewMatureSeq()' that finds the highest expressed 
	unannotated candidate and compares it with the annotated sequence.
	"""
	global unannotated, microRNA_map, hairpin_map
	
	# Find the highest expressed annotated miRNAs if any
	c3, c5 = 0,0
	annotated3, annotated5 = None, None

	for miRNA in microRNA_map[ref]:
		if miRNA.prime==3:
			c3 = miRNA.count
			annotated3 = miRNA
		elif miRNA.prime==5:
			c5 = miRNA.count
			annotated5 = miRNA


	# If no new unannotated sequences found for the hairpin, save mature sequence if any and set expression level of hairpin if true
	if ref not in unannotated:
		mature3 = annotated3.miRNA_id if c3!=0 else ''
		mature5 = annotated5.miRNA_id if c5!=0 else ''
		mostExpr=0
		if c3>c5:
			mostExpr=3
		elif c5!=0:
			mostExpr=5
		
		for miRNA in microRNA_map[ref]:
			if mature3!='': miRNA.setMature(mature3)
			if mature5!='': miRNA.setMature(mature5)
			if mostExpr!=0: miRNA.setHighestPrime(mostExpr)
		if mostExpr!=0:
			hairpin_map[ref].setExpressed()
		return 0

	highest3, highest5 = ['',0,0], ['',0,0] #[sequence,offset,count]
	for read in unannotated[ref]:
		seq = read[0]
		offset = read[1]
		count = read[2]
		prime = findPrime(ref, offset)
		unannotatedStrand = False
		if (prime==3 and annotated3==None) or (prime==5 and annotated5==None):
			unannotatedStrand = True

		# Check for new 3p sequence
		# Priority: count, offset diff, proximity to 22nt, end offset diff, shortest
		if prime==3 and count>c3:
			if count>highest3[2]:
				highest3 = [seq,offset,count]
			elif count==highest3[2]:
				if unannotatedStrand:
					lendiff_old = abs(22-len(highest3[0]))
					lendiff_new = abs(22-len(seq))
					if lendiff_new<lendiff_old:
						highest3=[seq,offset,count]
					continue
				else:
					startdiff_old = abs(annotated3.offset-highest3[1])
					startdiff_new = abs(annotated3.offset-offset)	
					if startdiff_new<startdiff_old:
						highest3=[seq,offset,count]
					elif startdiff_new==startdiff_old:
						lendiff_old = abs(22-len(highest3[0]))
						lendiff_new = abs(22-len(seq))
						if lendiff_new<lendiff_old:
							highest3=[seq,offset,count]
						elif lendiff_old==lendiff_new:
							enddiff_old = abs((annotated3.offset+len(annotated3.sequence))-(highest3[1]+len(highest3[0])))
							enddiff_new = abs((annotated3.offset+len(annotated3.sequence))-(offset+len(seq)))
							if enddiff_new<enddiff_old:
								highest3 = [seq,offset,count]
							elif enddiff_old==enddiff_new:
								len_old = len(highest3[0])
								len_new = len(seq)
								if len_new<len_old:
									highest3 = [seq,offset,count]
								
		elif prime==5 and count>c5: 
			if count>highest5[2]:
				highest5 = [seq,offset,count]
			elif count==highest5[2]:
				if unannotatedStrand:
					lendiff_old = abs(22-len(highest5[0]))
					lendiff_new = abs(22-len(seq))
					if lendiff_new<lendiff_old:
						highest5=[seq,offset,count]
					continue
				else:
					startdiff_old = abs(annotated5.offset-highest5[1])
					startdiff_new = abs(annotated5.offset-offset)
					if startdiff_new<startdiff_old:
						highest5=[seq,offset,count]
					elif startdiff_new==startdiff_old:
						lendiff_old = abs(22-len(highest5[0]))
						lendiff_new = abs(22-len(seq))
						if lendiff_new<lendiff_old:
							highest5=[seq,offset,count]
						elif lendiff_old==lendiff_new:
							enddiff_old = abs((annotated5.offset+len(annotated5.sequence))-(highest5[1]+len(highest5[0])))
							enddiff_new = abs((annotated5.offset+len(annotated5.sequence))-(offset+len(seq)))
							if enddiff_new<enddiff_old:
								highest5 = [seq,offset,count]
							elif enddiff_old==enddiff_new:
								len_old = len(highest5[0])
								len_new = len(seq)
								if len_new<len_old:
									highest5 = [seq,offset,count]
								
	

	news = 0
	mature3, mature5 = '', ''
	most3, most5 = 0,0
	if highest3[2]>c3:
		newSeq = highest3[0]
		newOffset = highest3[1]
		newCount = highest3[2]
		newPrime = 3
		newID = ref+'3N'
		microRNA_map[ref].append(MicroRNA(ref, newID, newCount, newOffset, newSeq, newPrime))
		mature3 = newID
		most3=newCount 
		news +=1
		#fout.write('%s\t%s\t%d\t%s\n' % (ref, newID, newOffset, newSeq))
	elif highest3[2]<c3:
		mature3 = annotated3.miRNA_id
		most3 = c3
		
	if highest5[2]>c5:
		newSeq = highest5[0]
		newOffset = highest5[1]
		newCount = highest5[2]
		newPrime = 5
		newID = ref+'5N'
		microRNA_map[ref].append(MicroRNA(ref, newID, newCount, newOffset, newSeq, newPrime))
		mature5 = newID
		most5 = newCount
		news += 1
		#fout.write('%s\t%s\t%d\t%s\n' % (ref, newID, newOffset, newSeq))
	elif highest5[2]<c5:
		mature5 = annotated5.miRNA_id
		most5 = c5

	mostExpr = 0
	if most3>most5:
		mostExpr=3
	elif most5>0:
		mostExpr = 5
	else:
		mostExpr = 0

	for miRNA in microRNA_map[ref]:
		if mature3!='': miRNA.setMature(mature3)
		if mature5!='': miRNA.setMature(mature5)
		if mostExpr!=0: miRNA.setHighestPrime(mostExpr)

	if mostExpr!=0:
		hairpin_map[ref].setExpressed()
	
	return news

def alignShortreadsToIsomirs():
	"""
	Alternative to alignShortreads which aligns shortreads to all isomirs and chooses the best fit. 
	"""
	global shortread_map, shortreads
	for ref in shortreads:
		for shortread in shortreads[ref]:
			diff=0.0
			position = ''
			mature = None
			
			offset = shortread[0]
			seq = shortread[1]
			count = shortread[2]
			endOffset = offset+len(seq)
			prime = findPrime(ref,offset)

			candidatemiRNAs = [miRNA for miRNA in microRNA_map[ref] if miRNA.prime==prime and miRNA.count>0.0]
			if len(candidatemiRNAs)==0:
				#No candidates -> no shortread
				continue
			elif len(candidatemiRNAs)==1:
				#One candidate -> new short read
				candidate = candidatemiRNAs[0]
				mature = candidate
				matOffset = candidate.offset
				matEndOffset = matOffset+len(candidate.sequence)
				diffStart = offset-matOffset
				diffEnd = endOffset-matEndOffset
				if abs(diffStart)<abs(diffEnd):
					position = 'start'
					diff=diffStart
				else:
					position='end'
					diff=diffEnd
			else:
				#Multiple candidates. Priority: Offset, RPM
				candidates = dict()
				for candidate in candidatemiRNAs:
					matOffset = candidate.offset
					matEndOffset = matOffset+len(candidate.sequence)
					diffStart = offset-matOffset
					diffEnd = endOffset-matEndOffset
					pos, diff = None, None
					if abs(diffStart)<abs(diffEnd):
						pos = 'start'
						diff=diffStart
					else:
						pos='end'
						diff=diffEnd
					if diff in candidates:
						candidates[diff].append((pos, candidate))
					else:
						candidates[diff] = [(pos, candidate)]
				
				diff = 100
				sameDiff = 100
				for key in candidates:
					if abs(key)<abs(diff):
						diff = key
					elif abs(key)==abs(diff):
						sameDiff = key
				if abs(diff)==abs(sameDiff):
					#Multiple candidates with same absolute value. Find candidate with highest RPM.
					
					highest = 0.0
					cand = None
					trueDiff = None
					for (position,miRNA) in candidates[diff]:
						if miRNA.count>highest:
							highest=miRNA.count
							cand=(position,miRNA)
							trueDiff = diff
					for (position,miRNA) in candidates[sameDiff]:
						if miRNA.count>highest:
							highest=miRNA.count
							cand=(position,miRNA)
							trueDiff = sameDiff

					position, mature = cand
					diff = trueDiff


				if len(candidates[diff])>1:
					#Multiple candidates with same diff. Find candidate with highest RPM
					highest = 0.0
					cand = None
					for (position,miRNA) in candidates[diff]:
						if miRNA.count>highest:
							highest=miRNA.count
							cand=(position,miRNA)
						elif miRNA.count==highest:
							if abs(22-len(cand[1].sequence)) > abs(22-len(miRNA.sequence)):
								highest=miRNA.count
								cand = (position, miRNA)
							elif abs(22-len(cand[1].sequence))==abs(22-len(miRNA.sequence)):
								if len(cand[1].sequence)>len(miRNA.sequence):
									highest=miRNA.count
									cand = (position,miRNA)

					
					#miRNA_highest is the new mature -> new short read
					position, mature = cand
				else:
					#Only one candidate -> new short read
					try:
						position = candidates[diff][0][0]
						mature = candidates[diff][0][1]
					except ValueError:
						print 'Got value error in alignShortreadsToIsomirs!'
						print candidates[diff]
						continue

			matOffset = mature.offset
			matEndOffset = matOffset+len(mature.sequence)

			if (prime==5 and ((endOffset - matOffset)<3)) or (prime==3 and ((matEndOffset - offset)<3)):
				mors[ref].append(shortread)
				continue			
		
			shortread_map[ref].append(Shortread(ref, offset, seq, count, mature.miRNA_id, position, prime, diff, mature.highest_prime))

def alignShortreads():
	"""
	To align short reads relative to its reference's most expressed mature sequence for the same strand.
	"""
	global shortreads, shortread_map
	for ref in hairpin_map:

		removed = 0
		for i in range(len(shortreads[ref])):
			read = i - removed
			srOffset = shortreads[ref][read][0]
			srSeq = shortreads[ref][read][1]
			srEnd = srOffset+len(srSeq)
			newPrime = findPrime(ref, srOffset)
			
			mature = None
			for miRNA in microRNA_map[ref]:
				if miRNA.prime==newPrime and miRNA.mature:
					mature = miRNA
			if mature==None:
				continue

			
			srCount = shortreads[ref][read][2]
			matOffset = mature.offset
			matEnd = matOffset+len(mature.sequence)
			diffStart = srOffset - matOffset
			diffEnd = srEnd-matEnd
			
			# Check if actually a MoR
			if (newPrime==5 and ((srEnd - matOffset)<3)) or (newPrime==3 and ((matEnd - srOffset)<3)):
				mors[ref].append(shortreads[ref][read])
				del shortreads[ref][read]
				removed += 1
				if len(shortreads[ref])==0:
					del shortreads[ref]
					break
				continue			
		
			pos = 'start' if newPrime==3 and abs(diffStart) < abs(diffEnd) or newPrime==5 and abs(diffStart) < abs(diffEnd) else 'end'
			diff = diffEnd if pos=='end' else diffStart
			shortread_map[ref].append(Shortread(ref, srOffset, srSeq, srCount, mature.miRNA_id, pos, newPrime, diff, mature.highest_prime))
			shortreads[ref][read].extend([mature.miRNA_id, mature.highest_prime, pos, diff, newPrime])
	
	
def isLoop(seq, i):
	chrs = seq[i+1:].replace('.', '')
	if chrs == '': return False
	return True if chrs[0] == ')' else False

def findPrime(ref, offset):
	"""
	Help method for finding what prime the sequence originates from.
	"""
	try:
		seq = hairpin_map[ref].foldstructure
	except KeyError:
		#print ('Could not obtain sequence for ref %s, exiting ' % ref)
		return -1
	loops = list()

	for i in range(len(seq)):
		if seq[i] == '(' and isLoop(seq, i):
			i5 = i
			i3 = seq[i:].find(')') + i
			mid = math.ceil((i3+i5)/2.0)
			loops.append(mid)
	
	if len(loops)==0:
		print('Could not find prime as loops is empty for %s' % ref)
		return -1
	
	if len(loops)>1:
		med = len(seq)/2
		mid = min(loops, key=lambda x:abs(x-med))
	else:
		mid = loops[0]

	if offset>mid: return 3
	if offset<=mid: return 5
		
def removeUnexpressed():
	"""
	To remove short read statistics for reference hairpins not expressed in sample data.
	"""
	global shortreads, shortread_map

	unexpressed = 0
	for ref in hairpin_map:
		expr = False

		for miRNA in microRNA_map[ref]:
			if miRNA.mature:
				expr = True
				continue
			if miRNA.count==0:
				miRNA.setUnexpressed()

		if not expr:
			unexpressed += 1
			del shortreads[ref]
			del shortread_map[ref]
			for miRNA in microRNA_map[ref]:
				miRNA.setUnexpressed()

def findHpStats(hairpins, shortreads):
	hp_no = len(hairpins)
	hp_expr = 0.0
	hp_sr = 0.0
	for ref in hairpins:
		if hairpins[ref].expressed:
			hp_expr += 1
			if ref not in shortreads:
				continue
			if shortreads[ref] != []:
				hp_sr += 1
	hp_expr_p = (hp_expr/hp_no)* 100
	hp_sr_p = (hp_sr/hp_expr) * 100

	return hp_no, hp_expr, hp_expr_p, hp_sr, hp_sr_p

def findMatStats(matures, shortreads):
	mat_no = 0
	mat_new = 0
	mat_expr = 0.0
	mat_sr = 0.0
	for ref in matures:
		for miRNA in matures[ref]:
			if miRNA.miRNA_id[-1] != 'N':
				mat_no += 1
				if miRNA.expressed:
					mat_expr += 1
			else:
				mat_new += 1
			if miRNA.expressed:
				sr = False
				for shortie in shortreads[ref]:
					if shortie.mature_id == miRNA.miRNA_id:
						sr = True
				if sr: mat_sr += 1
	mat_expr_tot = mat_expr+mat_new
	mat_expr_p = (mat_expr/mat_no)*100
	mat_sr_p = (mat_sr/mat_expr_tot)*100

	return mat_no, mat_expr, mat_expr_p, mat_new, mat_expr_tot, mat_sr, mat_sr_p

def findMatandShorties(matures, shortreads):
	mat_sr_high, mat_sr_low, mat_nosr_high, mat_nosr_low, mat_sr, mat_nosr = 0.0,0.0,0.0,0.0,0.0,0.0

	candidates = dict()

	for ref in matures:
		c3, c5 = 0.0, 0.0
		mat3, mat5 = None, None
		for miRNA in matures[ref]:
			if miRNA.prime==3 and miRNA.count > c3:
				mat3 = miRNA
				c3 = miRNA.count
			elif miRNA.prime==5 and miRNA.count > c5:
				mat5 = miRNA
				c5 = miRNA.count
		
		if c3> 0 and c5 > 0:
			candidates[ref] = [mat3, mat5]
		elif c3 > 0:
			candidates[ref] = [mat3]
		elif c5 > 0:
			candidates[ref] = [mat5]

	for ref in candidates:
		for miRNA in candidates[ref]:
			SR = False
			if ref in shortreads:
				for shortie in shortreads[ref]:
					if shortie.mature_id==miRNA.miRNA_id:
						SR = True
			if miRNA.highest_prime and SR:
				mat_sr_high += 1
			elif miRNA.highest_prime:
				mat_nosr_high += 1
			elif SR:
				mat_sr_low += 1
			else:
				mat_nosr_low += 1


	mat_sr = mat_sr_high + mat_sr_low
	mat_nosr = mat_nosr_high + mat_nosr_low
	return mat_sr_high, mat_sr_low, mat_nosr_high, mat_nosr_low, mat_sr, mat_nosr


def printReport(hairpins, matures, shortreads, names, total_name):
	print 'Writing report...'
	report = open(total_name+'_results/'+total_name+'_report.txt', 'w')
	report.write('REPORT FOR '+total_name+'\n\n')
	report.write('Hairpins\n\n')
	report.write('\tSample\t\tAnnotated\tExpressed\t\t\tShort read associated\n')
	report.write('\t.................................................................\n')

	for i in range(len(hairpins)):
		hp_no, hp_expr, hp_expr_p, hp_sr, hp_sr_p = findHpStats(hairpins[i],shortreads[i])
		numberline = '\t%s\t\t%d\t\t%d ( %.2f %% ) \t%d ( %.2f %% )\n' % (names[i], hp_no, hp_expr, hp_expr_p, hp_sr, hp_sr_p)
		report.write(numberline)
	
	report.write('\n\nmiRNAs\n\n')
	report.write('\tSample\t\tAnnotated\tExpressed\t\t\tUnannotated\t\tTotal expressed\t\tShort read associated\n')
	report.write('\t.....................................................................................................\n')
	for i in range(len(matures)):
		mat_no, mat_expr, mat_expr_p, mat_new, mat_expr_tot, mat_sr, mat_sr_p = findMatStats(matures[i], shortreads[i])
		numberline = '\t%s\t\t%d\t\t%d ( %.2f %% )  \t%d\t\t\t\t%d\t\t\t\t\t%d ( %.2f %% ) \n' % (names[i], mat_no, mat_expr, mat_expr_p, mat_new, mat_expr_tot, mat_sr, mat_sr_p)
		report.write(numberline)

	report.write('\n\nShort read associated miRNA statistics\n\n')
	report.write('\t\t\t\t\t\tmiRNAs with short reads\t\t\t\t\t    miRNAs without short reads\t\n')
	report.write('\tSample\t\tHighest strand\t\tLowest strand\tSum\t\tHighest strand\t\tLowest strand\t\tSum\n')
	report.write('\t...................................................................................................\n')
	for i in range(len(matures)):
		mat_sr_high, mat_sr_low, mat_nosr_high, mat_nosr_low, mat_sr, mat_nosr = findMatandShorties(matures[i], shortreads[i])
		mat_sr_high_p = (mat_sr_high/mat_sr)*100
		mat_sr_low_p = (mat_sr_low/mat_sr)*100
		mat_nosr_high_p = (mat_nosr_high/mat_nosr)*100
		mat_nosr_low_p = (mat_nosr_low/mat_nosr)*100

		numberline = '\t%s\t\t%d ( %.2f %% )\t\t%d ( %.2f %% )\t%d\t\t%d ( %.2f %% )\t\t%d ( %.2f %% )\t\t%d\n' % (names[i],mat_sr_high, mat_sr_high_p, mat_sr_low, mat_sr_low_p, mat_sr, mat_nosr_high, mat_nosr_high_p, mat_nosr_low, mat_nosr_low_p, mat_nosr)
		report.write(numberline)

	report.close()

if __name__ == '__main__':
	"""
	Called when run in command mode.
	Input format: 'python produceShortreadReport hairpin_refs foldstructures mirna_refs name input_sample_1 sample_name_1 input_sample_2 sample_name_2 .... input_sample_n sample_name_n '
	"""
	total_shortreads, total_matures, total_hairpins, all_names = [], [], [], []

	references = sys.argv[1]
	folds = sys.argv[2]
	matures = sys.argv[3]
	total_name = sys.argv[4]

	for i in range(5, len(sys.argv), 2):
		sample = sys.argv[i]
		name = sys.argv[i+1]
		new_SR, new_mat, new_hp = main(references, folds, matures, sample, name, total_name)
		total_shortreads.append(new_SR), total_matures.append(new_mat), total_hairpins.append(new_hp), all_names.append(name)

	if not os.path.exists(total_name+'_results'):
  	 	os.makedirs(total_name+'_results')

  	#Analyses
	pssf.plotRegression(total_matures, total_shortreads, all_names, total_name)
	ca.produceAnovaData(total_shortreads, all_names, total_name+'_results/'+total_name)
	printReport(total_hairpins, total_matures, total_shortreads, all_names, total_name)
	fmc.findMiRNAClassifications(total_shortreads, total_matures, all_names, total_name)
	fnp.findNucleotidePreferences(total_shortreads, total_matures, all_names, total_name)
	fb.findBins(total_matures, total_shortreads, all_names, total_name)
	fl.findLengths(total_matures, total_shortreads, all_names, total_name)
	fa.findAlignments(total_shortreads, all_names, total_name)

