import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy import stats 

def findStrandPrefClasses(shortread_list, miRNA_list, names, total_name):
	"""
	Main method.
	Splits all miRNAs into two classes, and plots their corresponding SR-association for each strand.
	"""
	plotShortReads(shortread_list, miRNA_list, names, total_name, 'start')
	plotShortReads(shortread_list, miRNA_list, names, total_name, 'end')

def plotShortReads(shortread_list, miRNA_list, names, total_name, position):
	low_pairs_diff, high_pairs_diff = list(), list()
	low_pairs_eq, high_pairs_eq = list(), list()
	both_srpairs_diff, both_nosrpairs_diff = list(), list()
	both_srpairs_eq, both_nosrpairs_eq = list(), list()
	diffDataTotal_pairs, diffDataTotal_both, eqDataTotal_pairs, eqDataTotal_both = [[],[],[],[]],[[],[],[],[]],[[],[],[],[]],[[],[],[],[]]
	
	for i in range(len(miRNA_list)):
		different_class, equal_class = findMiRNAClassesTotalSum(miRNA_list[i])
		diffData_i = prepareData('Different',different_class, shortread_list[i], miRNA_list[i], position)
		low_pair_diff, high_pair_diff, both_srpair_diff, both_nosrpair_diff = investigateData(different_class, 'Different', shortread_list[i], miRNA_list[i], position, total_name)
		low_pairs_diff.extend(low_pair_diff)
		high_pairs_diff.extend(high_pair_diff)
		both_srpairs_diff.extend(both_srpair_diff)
		both_nosrpairs_diff.extend(both_nosrpair_diff)
		low_pair_eq, high_pair_eq, both_srpair_eq, both_nosrpair_eq = investigateData(equal_class, 'Equal', shortread_list[i], miRNA_list[i], position, total_name)
		low_pairs_eq.extend(low_pair_eq)
		high_pairs_eq.extend(high_pair_eq)
		both_srpairs_eq.extend(both_srpair_eq)
		both_nosrpairs_eq.extend(both_nosrpair_eq)
		eqData_i = prepareData('Equal',equal_class, shortread_list[i], miRNA_list[i], position)

		for j in range(0,4):
			diffDataTotal_pairs[j].extend(diffData_i[j])
			eqDataTotal_pairs[j].extend(eqData_i[j])
			diffDataTotal_both[j].extend(diffData_i[j+4])
			eqDataTotal_both[j].extend(eqData_i[j+4])

	plotPairs(low_pairs_diff, high_pairs_diff, total_name, position, 'Different')
	plotPairsBoth(both_srpairs_diff, both_nosrpairs_diff, total_name, position, 'Different')
	plotPairs(low_pairs_eq, high_pairs_eq, total_name, position, 'Equal')
	plotPairsBoth(both_srpairs_eq, both_nosrpairs_eq, total_name, position, 'Equal')
	
	plotBoxplots(diffDataTotal_pairs, total_name, position, 'Different', pairs=True)
	plotBoxplots(diffDataTotal_both, total_name, position, 'Different', pairs=False)
	plotBoxplots(eqDataTotal_pairs, total_name, position, 'Equal', pairs=True)
	plotBoxplots(eqDataTotal_both, total_name, position, 'Equal', pairs=False)

def plotBoxplots(data, total_name, position, miRNAclass, pairs=None):
	if pairs:
		labels = ['Lowest w/SR', 'Highest wo/SR', 'Lowest wo/SR', 'Highest w/SR']
	else:
		labels = ['Lowest w/SR', 'Highest w/SR', 'Lowest wo/SR', 'Highest wo/SR']
	
	ind = np.arange(len(labels))
	fig = plt.figure()
	ax = fig.add_subplot(111)

	data_boxes = ax.boxplot(data, notch=True, positions=ind,  patch_artist=True, sym='o')
	
	if pairs:
		edgeandfacecolors = [['#66c2a5','#b3e2cd'],['#66c2a5','#b3e2cd'],['#80b1d3','#a6cee3'],['#80b1d3','#a6cee3']]
		edgecolors = ['#66c2a5','#66c2a5','#66c2a5','#66c2a5','#80b1d3','#80b1d3','#80b1d3','#80b1d3']
	else:
		edgeandfacecolors = [['#fc8d62','#fed9a6'],['#fc8d62','#fed9a6'],['#fb8072','#fbb4ae'],['#fb8072','#fbb4ae']]
		edgecolors = ['#fc8d62','#fc8d62','#fc8d62','#fc8d62','#fb8072','#fb8072','#fb8072','#fb8072']

	for box,colors in zip(data_boxes['boxes'],edgeandfacecolors):
		box.set(color=colors[0], facecolor=colors[1])

	for whisk,colors in zip(data_boxes['whiskers'],edgecolors):
		whisk.set(color=colors, linewidth=2)

	for median in data_boxes['medians']:
		if pairs:
			median.set(color='#fb8072', linewidth=2)
		else:
			median.set(color='#66a61e' , linewidth=2)

	for cap,colors in zip(data_boxes['caps'],edgecolors):
		cap.set(color=colors, linewidth=2)

	for sym,colors in zip(data_boxes['fliers'],edgecolors):
		sym.set(color=colors, alpha=0.6)

	ymin, ymax = ax.get_ylim()
	for i in range(len(labels)):
		ax.text(ind[i]-0.2, ymax, 'n=%d'%len(data[i]), fontsize=25)

	ax.set_xticks(ind, minor=False)
	ax.set_xticklabels(labels, rotation=40, ha='right')
	ax.tick_params(axis='x', labelsize=25)
	ax.tick_params(axis='y', labelsize=25)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.yaxis.grid(True) 
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.yaxis.labelpad = 10
	ax.set_ylabel('Log$_2$ hairpin rpm', fontsize=30)
	plt.subplots_adjust(left=0.18, right=0.9, top=0.85, bottom=0.33)
	
	if pairs:
		plt.savefig('%s_results/miRNAClasses/%s_%s_%s_boxplot_pairs.png' % (total_name, total_name, position, miRNAclass))
	else:
		plt.savefig('%s_results/miRNAClasses/%s_%s_%s_boxplot_both.png' % (total_name, total_name, position, miRNAclass))
	plt.clf()


def findMiRNAClassesTotalSum(miRNAs):
	"""
	Classifies hairpins based on the sum of miRNAs on each strand.
	Returns two classes, 'Different' and 'Equal'
	"""
	different_class, equal_class = dict(), dict()
	for ref in miRNAs:
		p5total, p3total = 0.0, 0.0
		for miRNA in miRNAs[ref]:
			if miRNA.prime==5 and miRNA.count>0.5:
				p5total += miRNA.count
			elif miRNA.prime==3 and miRNA.count>0.5:
				p3total += miRNA.count
		if p5total==0.0 or p3total==0.0:
			continue
		diff = max(p5total,p3total)/min(p5total,p3total)
		strand = 3 if max(p5total,p3total)==p3total else 5		
		if diff<10:
			equal_class[ref] = strand
		else:
			different_class[ref] = strand
	return different_class, equal_class

def prepareData(className,miRNA_class, shortreads, miRNAs, pos):
	"""
	Returns the data for boxplots in 6 groups. 2 main groups, based on whether the hairpin has any shortreads associated with it or not.
	Within each SR/noSR group, the total hairpin expression data for lowest, highest and both is returned.
	"""
	lowest, lowestSR, highest, highestSR, both_low, both_lowSR, both_high, both_highSR = list(), list(), list(), list(), list(), list(), list(), list()
	for ref in miRNA_class:
		#Sum total expression on each strand for hairpin
		lowSR, highSR = False, False
		if ref in shortreads:
			for shortie in shortreads[ref]:
				if shortie.position!=pos or shortie.count<0.5:
					continue
				if shortie.prime==miRNA_class[ref]:
					highSR = True
				else:
					lowSR = True

		low, high = 0., 0.
		for miRNA in miRNAs[ref]:
			if miRNA.prime==miRNA_class[ref] and miRNA.count>=0.5:
				high += miRNA.count
			elif miRNA.count>=0.5:
				low += miRNA.count

		if lowSR and highSR:
			both_highSR.append(math.log(high,2))
			both_lowSR.append(math.log(low,2))
		elif lowSR:
			lowestSR.append(math.log(low,2))
			highest.append(math.log(high,2))
		elif highSR:
			highestSR.append(math.log(high,2))
			try:
				lowest.append(math.log(low,2))
			except:
				pass
		else:
			both_high.append(math.log(high,2))
			try:
				both_low.append(math.log(low,2))
			except:
				pass
	return [lowestSR, highest, lowest, highestSR, both_lowSR, both_highSR, both_low, both_high]

def investigateData(miRNA_class, className, shortreads, miRNAs, pos, total_name):
	high_pair, low_pair, both_srpair, both_nosrpair = list(), list(), list(), list()

	for ref in miRNA_class:
	
		#Sum total expression on each strand for hairpin
		lowSR, highSR = False, False
		
		if ref in shortreads:
			for shortie in shortreads[ref]:
				if shortie.position!=pos or shortie.count<0.5:
					continue
				if shortie.prime==miRNA_class[ref]:
					highSR = True
				else:
					lowSR = True
		low, high = 0., 0.
		for miRNA in miRNAs[ref]:
			if miRNA.prime==miRNA_class[ref] and miRNA.count>=0.5:
				high += miRNA.count
			elif miRNA.count>=0.5:
				low += miRNA.count

		if lowSR and highSR:
			both_srpair.append((math.log((high/low),2), math.log(high,2)))
		elif lowSR:
			low_pair.append((math.log((high/low),2),math.log(high,2)))
		elif highSR:
			high_pair.append((math.log((high/low),2),math.log(high,2)))
		else:
			both_nosrpair.append((math.log((high/low),2), math.log(high,2)))
	return low_pair, high_pair, both_srpair, both_nosrpair

def plotPairs(low_pair, high_pair, total_name, pos, miRNA_class):
	try:
		yl, xl = zip(*low_pair)
	except:
		yl, xl = [],[]
	try:
		yh, xh = zip(*high_pair)
	except:
		yh, xh = [],[]
	fig = plt.figure()
	ax_l = fig.add_subplot(121)
	ax_h = fig.add_subplot(122)
	plt.subplots_adjust(left=0.2, right=0.9, top=0.95, bottom=0.15)
	ax_l.scatter(xl, yl, alpha=0.6, s=200, c='#b3e2cd')
	ax_h.scatter(xh, yh, alpha=0.6, s=200, c='#a6cee3')
	ax_l.yaxis.labelpad = 20
	ax_l.xaxis.labelpad = 15
	ax_h.xaxis.labelpad = 15
	ax_l.set_ylabel('log$_2$(highest/lowest)', fontsize=25)
	ax_l.set_xlabel('log$_2$(highest)', fontsize=20)
	ax_h.set_xlabel('log$_2$(highest)', fontsize=20)
	ax_l.spines['top'].set_visible(False)
	ax_l.spines['right'].set_visible(False)
	ax_h.spines['top'].set_visible(False)
	ax_h.spines['right'].set_visible(False)
	ax_l.xaxis.set_ticks_position('bottom')
	ax_l.yaxis.set_ticks_position('left')
	ax_h.xaxis.set_ticks_position('bottom')
	ax_h.yaxis.set_ticks_position('left')
	ax_l.tick_params(axis='x', labelsize=17)
	ax_l.tick_params(axis='y', labelsize=17)
	ax_h.tick_params(axis='x', labelsize=17)
	ax_h.tick_params(axis='y', labelsize=17)
	plt.savefig('%s_results/miRNAClasses/%s_%s_FC_%s_pairs.png'%(total_name, total_name, pos, miRNA_class))
	plt.clf()

def plotPairsBoth(low_pair, high_pair, total_name, pos, miRNA_class):
	try:
		yl, xl = zip(*low_pair)
	except:
		yl, xl = [],[]
	try:
		yh, xh = zip(*high_pair)
	except:
		yh, xh = [],[]
	fig = plt.figure()
	ax_l = fig.add_subplot(121)
	ax_h = fig.add_subplot(122)
	plt.subplots_adjust(left=0.2, right=0.9, top=0.95, bottom=0.15)
	ax_l.scatter(xl, yl, alpha=0.6, s=200, c='#fed9a6')
	ax_h.scatter(xh, yh, alpha=0.6, s=200, c='#fbb4ae')
	ax_l.yaxis.labelpad = 20
	ax_l.xaxis.labelpad = 15
	ax_h.xaxis.labelpad = 15
	ax_l.set_ylabel('log$_2$(highest/lowest)', fontsize=25)
	ax_l.set_xlabel('log$_2$(highest)', fontsize=20)
	ax_h.set_xlabel('log$_2$(highest)', fontsize=20)
	ax_l.spines['top'].set_visible(False)
	ax_l.spines['right'].set_visible(False)
	ax_h.spines['top'].set_visible(False)
	ax_h.spines['right'].set_visible(False)
	ax_l.xaxis.set_ticks_position('bottom')
	ax_l.yaxis.set_ticks_position('left')
	ax_h.xaxis.set_ticks_position('bottom')
	ax_h.yaxis.set_ticks_position('left')
	ax_l.tick_params(axis='x', labelsize=17)
	ax_l.tick_params(axis='y', labelsize=17)
	ax_h.tick_params(axis='x', labelsize=17)
	ax_h.tick_params(axis='y', labelsize=17)
	plt.savefig('%s_results/miRNAClasses/%s_%s_FC_%s_both.png'%(total_name, total_name, pos, miRNA_class))
	plt.clf()
	
