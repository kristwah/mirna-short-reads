import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np

def plotRegression(matures, shortreads, name, total_name):
	diffs = dict()
	
	for ref in shortreads:
		for shortie in shortreads[ref]:
			if shortie.position!='end' or shortie.count<0.5:
				continue
			for miRNA in matures[ref]:
				if miRNA.miRNA_id==shortie.mature_id and miRNA.count>=0.5:
					diff = len(miRNA.sequence)-len(shortie.sequence)
					if diff in diffs:
						diffs[diff] += shortie.count
					else:
						diffs[diff] = shortie.count

	labels, values = list(), list()
	for key in sorted(diffs):
		labels.append(key)
		values.append(diffs[key])
	ind = np.arange(len(values))
	fig = plt.figure()
	ax = fig.add_subplot(111)
	plt.subplots_adjust(left=0.15, right=0.95, top=0.95, bottom=0.15)
	ax.bar(ind, values, color='#3182bd')
	ax.set_xticks(ind)
	ax.set_xticklabels(labels)
	ax.tick_params(axis='x', labelsize=20)
	ax.tick_params(axis='y', labelsize=20)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.set_ylabel('End read expression, rpm', fontsize=25)
	ax.set_xlabel('Length differences', fontsize=25)
	plt.savefig('%s_results/SRvsLength/%s_%s_ERvsMatLengthDiff.png'%(total_name, total_name, name))
	plt.clf()

