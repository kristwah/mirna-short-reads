import plotERvsMatLength as pevml 
import plotShortreadRPMvsLength as psrvl
import plotERvsMatLengthTotal as pevmlt
import os
import math
def findLengths(total_matures, total_shortreads, names, total_name):
	savepath = total_name+'_results/SRvsLength'
	print 'Plotting lengths...'

	if not os.path.exists(savepath):
		os.makedirs(savepath)
	pevmlt.plotAll(total_matures, total_shortreads, total_name)

	for i in range(len(names)):
		pevml.plotRegression(total_matures[i], total_shortreads[i], names[i], total_name)
		psrvl.plotLengths(total_shortreads[i], names[i], total_name)