
def produceAnovaData(shortreads, names, savename):
	"""
	This script retrieves short reads and prints them in a table format to be used in r for calculating anovas.
	See anova-r.script for details on further processing.
	"""

	t = open(savename+'_anovadata.txt', 'w')
	newLine = ['ago', 'strand', 'position', 'offset', 'count']
	t.write('\t'.join(newLine))
	t.write('\n')

	for i,shortie in enumerate(shortreads):
		name = names[i]
		for ref in shortie:
			for Shortread in shortie[ref]:
				newLine = [name, str(Shortread.prime), Shortread.position, str(Shortread.mat_offset), str(Shortread.count)]
				t.write('\t'.join(newLine))
				t.write('\n')
	t.close()
