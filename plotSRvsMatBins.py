import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import numpy as np

def plotBins(matures, shortreads, name, total_name):
	"""
	Method for plotting SR-associated miRNAs as a binned histogram.
	"""
	bins = createBins(matures)
	barData = createBars(bins, shortreads)
	topData = [100,100,100,100,100,100,100,100,100,100]

	fig = plt.figure()
	ax = fig.add_subplot(111)
	ind = np.arange(10)

	plt.bar(ind, height=topData, color='#c6dbef')
	plt.bar(ind, height=barData, color='#045a8d')
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.set_xticklabels([])
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.tick_params(axis='y', labelsize=20)
	ax.yaxis.labelpad = 10
	plt.subplots_adjust(left=0.15, right=0.9, top=0.9, bottom=0.1)
	ax.set_ylabel('SR-association of miRNAs, %', fontsize=25)
	ax.set_xlabel('low        miRNA expression      high', fontsize=25)
	plt.savefig('%s_results/Bins/%s_%s_SRvsMat_bins.png' % (total_name, total_name, name))
	plt.clf()


def createBins(matures):
	"""
	Method for calculating the bins.
	"""
	miRNAs = list()

	for ref in matures:
		for miRNA in matures[ref]:
			if miRNA.count>=0.5:
				miRNAs.append([miRNA.count, miRNA])

	miRNAs.sort()
	splitsize = 1.0/10*len(miRNAs)
	bins = [(miRNAs[int(round(i*splitsize)):int(round((i+1)*splitsize))]) for i in range(10)]

	return bins


def createBars(bins, shortreads):
	"""
	Method for calculating the SR-associated percentage data for the plot.
	Returns a list of ten values, each representing the percentage of it's corresponding bin's SR-associated miRNAs.
	"""
	bars = list()

	for bin in bins:
		miRNAs = list()
		SR_count = 0.0
		for item in bin:
			SR = False
			miRNAs.append(item[1].miRNA_id)
			for ref in shortreads:
				for shortie in shortreads[ref]:
					if shortie.mature_id==item[1].miRNA_id and shortie.count>=0.5:
					 	SR = True
			if SR: SR_count += 1

		percentage = SR_count/len(miRNAs)*100.0
		bars.append(percentage)

	return bars

