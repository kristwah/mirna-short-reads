import plotSRvsMatBins as psvmb 
import plotSRvsMatBinsSplit as psvmbs
import os 

def findBins(matures, shortreads, names, total_name):
	print 'Plotting bins...'
	savepath = '%s_results/Bins' % total_name
	if not os.path.exists(savepath):
		os.makedirs(savepath)
	for i in range(len(names)):
		psvmb.plotBins(matures[i], shortreads[i], names[i], total_name)
		psvmbs.plotBins(matures[i], shortreads[i], names[i], total_name)
