import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np 


def plotAlignments(shortreads, total_name, name=None):
	savename = total_name+'_results/Alignments/'+total_name

	tot_labels, tot_start, tot_end = generateListsBar(shortreads, 0)
	p3_labels, p3_start, p3_end = generateListsBar(shortreads, 3)
	p5_labels, p5_start, p5_end = generateListsBar(shortreads, 5)

	if name==None:
		plot(tot_labels, tot_start, tot_end, total_name, savename)
		plot(p3_labels, p3_start, p3_end, total_name+' 3p', savename+'_3p')
		plot(p5_labels, p5_start, p5_end, total_name+' 5p', savename+'_5p')
	else:
		plot(tot_labels, tot_start, tot_end, '%s %s' %(total_name, name), '%s_%s'%(savename, name))
		plot(p3_labels, p3_start, p3_end, '%s %s 3p'%(total_name, name), '%s_%s_3p'%(savename, name))
		plot(p5_labels, p5_start, p5_end, '%s %s 5p'%(total_name, name), '%s_%s_5p'%(savename, name))

def plot(labels, start, end, title, name):
	width = 0.4
	ind = np.arange(11)
	fig = plt.figure()
	ax = fig.add_subplot(111)
	try:
		ax.bar(ind, start, width, color='#92c5de', edgecolor='#2166ac', label='Start offset')
	 	ax.bar(ind, end, width, color='#f4a582', edgecolor='#b2182b', label='End offset', bottom=start)
	except:
		print 'Error when trying to plot alignment bars'
		return

	ax.legend(loc='upper left', fontsize=20)
	ax.set_xticks(ind+0.5*width, minor=False)
	ax.set_xticklabels(labels, minor=False, fontdict=False, fontsize=18)
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.set_ylabel('Short read expression', fontsize=28)
	ax.yaxis.labelpad = 10
	ax.tick_params(axis='y', labelsize=20)
	plt.subplots_adjust(left=0.2, right=0.95, top=0.95, bottom=0.1)	
	plt.savefig(name+'_SRalignments.png')
	plt.clf()

def generateListsBar(shortread_maps, prime):
	"""
	Generete a start and end list where every offset outside [-2,2] is 'others'. Starts are [0,1,2,others], ends are [others, 2, 1, 0].
	"""
	stat = {-2:[0,0], -1:[0,0], 0:[0,0], 1:[0,0], 2:[0,0], 'other':[0,0]}
	labels, start, end = [], [], []

	for shortread_map in shortread_maps:
		for ref in shortread_map:
			for Shortread in shortread_map[ref]:
				if (prime==5 and Shortread.prime==3) or (prime==3 and Shortread.prime==5):
					continue
				if Shortread.position=='start':
					offset = Shortread.mat_offset
					count = Shortread.count
					if offset not in range(-2,3):
						offset='other'
					stat[offset][0] += count
				elif Shortread.position=='end':
					offset = Shortread.mat_offset
					count = Shortread.count
					if offset not in range(-2,3):
						offset = 'other'
					stat[offset][1] += count
					

	for key, value in iter(sorted(stat.iteritems())):
		start.append(value[0])
		end.append(value[1])
		labels.append(key)

	while len(start)<11:
		start.extend([0])
	while len(end)<11:
		end.extend([0])
	end.reverse()

	labels = ['-2', '-1', '0', '1', '2', 'other', '2', '1', '0', '-1', '-2']
	return labels, start, end

