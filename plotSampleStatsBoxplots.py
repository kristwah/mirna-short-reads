import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plot
import numpy as np
import math

def plotAlignments(shortreads, total_name, name=None):
	savename = total_name+'_results/Alignments/'+total_name
	start3, end3, labels3 = createData(shortreads, 3)
	start5, end5, labels5 = createData(shortreads, 5)
	start_tot, end_tot, labels_tot = createData(shortreads, 0)
	
	if name==None:
		plotBoxplots(start3, end3, labels3, total_name+' 3p', savename+'_3p')
		plotBoxplots(start5, end5, labels5, total_name+' 5p', savename+'_5p')
		plotBoxplots(start_tot, end_tot, labels_tot, total_name, savename)
	else:
		plotBoxplots(start3, end3, labels3, '%s %s 3p' % (total_name, name), '%s_%s_3p' % (savename, name))
		plotBoxplots(start5, end5, labels5, '%s %s 5p' % (total_name, name), '%s_%s_5p' % (savename, name))
		plotBoxplots(start_tot, end_tot, labels_tot, '%s %s' % (total_name, name), '%s_%s' % (savename, name))


def plotBoxplots(start, end, labels, name, savename):
	ind = np.arange(len(labels))
	width = 0.2

	fig = plot.figure()
	ax = fig.add_subplot(111)
	bp_start = ax.boxplot(start, positions=ind, widths = width, patch_artist=True, sym='o')
	bp_end = ax.boxplot(end, positions=ind+width, widths=width, patch_artist=True, sym='o')
	
	for box in bp_start['boxes']:
		box.set(color='#2166ac', facecolor='#92c5de')
	for box in bp_end['boxes']:
		box.set(facecolor='#f4a582', color='#b2182b')

	for whisk in bp_start['whiskers']:
		whisk.set(color='#2166ac', linewidth=2)
	for whisk in bp_end['whiskers']:
		whisk.set(color='#b2182b', linewidth=2)

	for median in bp_start['medians']:
		median.set(color='#2166ac', linewidth=2)
	for median in bp_end['medians']:
		median.set(color='#b2182b', linewidth=2)

	for cap in bp_start['caps']:
		cap.set(color='#2166ac', linewidth=2)
	for cap in bp_end['caps']:
		cap.set(color='#b2182b', linewidth=2)

	for sym in bp_start['fliers']:
		sym.set(color='#2166ac', alpha=0.6)
	for sym in bp_end['fliers']:
		sym.set(color='#b2182b', alpha=0.6)

	ax.set_xticks(ind+width*0.5, minor=False)
	ax.set_xticklabels(labels, minor=False, fontdict=False, fontsize=25)
	ax.tick_params(axis='y')
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.set_ylabel('log$_2$ short read expression', fontsize=27)
	ax.yaxis.labelpad = 10
	ax.tick_params(axis='y', labelsize=25)
	plot.subplots_adjust(left=0.2, right=0.95, top=0.95, bottom=0.1)	
	startRect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#2166ac', fc='#92c5de')
	endRect = mpl.patches.Rectangle([0.025, 0.05], 0.05, 0.1, ec='#b2182b', fc='#f4a582')
	ax.legend([startRect, endRect], ['Start', 'End'], fontsize=20)
	fig.savefig(savename+'_SRalignments_quartile.png')
	plot.clf()

def createData(shortread_maps, prime):
	stat = {}
	labels, start, end = [], [], []

	for shortread_map in shortread_maps:
		for ref in shortread_map:
			for Shortread in shortread_map[ref]:
				if (prime==5 and Shortread.prime==3) or (prime==3 and Shortread.prime==5):
					continue
				count = math.log(Shortread.count, 2)
				if Shortread.position=='start':
					offset = Shortread.mat_offset
					if abs(offset)>2:
						offset='other'
					if offset in stat:
						stat[offset][0].append(count)
					else:
						stat[offset]=[[count], []]
				elif Shortread.position=='end':
					offset = Shortread.mat_offset
					if abs(offset)>2:
						offset = 'other'
					if offset in stat:
						stat[offset][1].append(count)
					else:
						stat[offset]=[[],[count]]

	for key, value in iter(sorted(stat.iteritems())):
		start.append(value[0])
		end.append(value[1])
		labels.append(key)

	return start, end, labels
