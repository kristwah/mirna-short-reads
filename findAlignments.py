import plotSampleStats as pss 
import plotSampleStatsBoxplots as pssb 
import os

def findAlignments(total_shortreads, names, total_name):
	savepath = total_name+'_results/Alignments'
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	print 'Plotting alignments...'
	for i in range(len(names)):
		pss.plotAlignments([total_shortreads[i]], total_name, name=names[i])
		pssb.plotAlignments([total_shortreads[i]], total_name, name=names[i])

	pss.plotAlignments(total_shortreads, total_name)
	pssb.plotAlignments(total_shortreads, total_name)
