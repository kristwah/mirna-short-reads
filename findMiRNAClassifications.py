import findStrandPrefMiRNAClasses as fspmc 
import findStrandPrefMiRNAClassesTotal as fspmct 
import plotStrandPrefMiRNAboxplots as pspmb
import plotStrandPrefMiRNAboxplots_total as pspmbt
import os

def findMiRNAClassifications(total_shortreads, total_matures, all_names, total_name):
	print 'Plotting miRNA classifications...'

	savepath = total_name+'_results/miRNAClasses'
	if not os.path.exists(savepath):
		os.makedirs(savepath)

	# Percentage and rpm graphs for pairs. 
	fspmc.findStrandPrefClasses(total_shortreads, total_matures, all_names, total_name)
	fspmct.findStrandPrefClasses(total_shortreads, total_matures, all_names, total_name)	
	# Boxplots
	# Different boxplots w/notches and plotted in pairs. Separately for start/end
	pspmb.findStrandPrefClasses(total_shortreads, total_matures, all_names, total_name)
	# Different boxplots w/notches and plotted in pairs. Start/end combined
	pspmbt.findStrandPrefClasses(total_shortreads, total_matures, all_names, total_name)