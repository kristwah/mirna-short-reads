# READ ME #

Main file to run is the produceShortreadReport.py file, to be run in command line mode:
"python produceShortreadReport.py hairpin_references hairpin_foldstructure mirna_references name sample1 sample1_name sample2 sample2_name ... samplen samplen_name"

Reference input files are found in 'Inputfiles', and contain hairpin references, foldstrutures and miRNA references for human genome (prefix 'hsa') and mouse genome (prefix 'mmu').

Sample data sets are found in folders for 'Corry', 'Daub', 'Lundbaek', 'Meister', 'Rui' and 'Rajewsky'.

All analyses are initiated from the produceShortreadReport.py file, through the following files:
	findAlignments.py 				Plots the alignments
	plotSampleSRvsFull.py 			Plots the correlation of expression of short reads and miRNAs
	calcAnova.py 					Creates data to be used as input in ANOVA analyses in R
	findMiRNAClassifications.py 	Runs all analyses related to the classification of hairpins based on their strand preference
	findBins.py 					Plots the short read association of miRNAs related to their expression level
	findLengths.py 					Runs all analyses of short read lengths and differences between end reads and miRNAs
	findNucleotidePreferences.py 	Plots the terminal nucleotide distributions of miRNAs

The output of all analyses will be structured in separate folders under a 'name_results' folder, given by the input name.

Whether the analyses should be run for mature sequences only or all expressed isomiRs must be defined in the produceShortreadReport.py file, in the main() method and readSample() method, see comments in the code. The default setting is to only regard mature sequences.