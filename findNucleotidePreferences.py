import plotNucleotidePreferences as pnps
import plotNucleotidePreferencesRPM as pnpsrpm
import plotNucleotidePreferencesMatvsIsomirs as pnpmi
import plotNucleotidePreferencesMatvsIsomirsRPM as pnpmirpm
import os

def findNucleotidePreferences(total_shortreads, total_matures, all_names, total_name):
	savepath = total_name+'_results/ntPreferences'
	if not os.path.exists(savepath):
		os.makedirs(savepath)

  	print 'Plotting nucleotide preferences...'

	pnps.plotNucleotides(total_shortreads, total_matures, all_names, total_name)
	pnpsrpm.plotNucleotides(total_shortreads, total_matures, all_names, total_name)
	pnpmi.plotNucleotides(total_matures, all_names, total_name)
	pnpmirpm.plotNucleotides(total_matures, all_names, total_name)
	
